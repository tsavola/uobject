#
# Copyright 2006  Timo Savola
# Distributed under the Boost Software License, Version 1.0.
#

PREFIX		= /usr/local
DESTDIR		= 

CC		= cc
AR		= ar
RANLIB		= ranlib
INSTALL		= install
DOXYGEN		= doxygen

CPPFLAGS	= 
CFLAGS		= -Wall -Wextra -pedantic -std=c99
LDFLAGS		= 

OPT_CPPFLAGS	= $(CPPFLAGS) -DNDEBUG
OPT_CFLAGS	= $(CFLAGS) -O2 -fomit-frame-pointer -Wno-unused
OPT_LDFLAGS	= $(LDFLAGS) -s

DBG_CPPFLAGS	= $(CPPFLAGS)
DBG_CFLAGS	= $(CFLAGS) -g
DBG_LDFLAGS	= $(LDFLAGS)

VERSION		= 
SOVERSION	= 0

SOURCES		= $(wildcard uobject/*.c)

OPT_PIC_OBJECTS	= $(SOURCES:uobject/%.c=obj/opt/%.os)
OPT_OBJECTS	= $(SOURCES:uobject/%.c=obj/opt/%.o)
DBG_OBJECTS	= $(SOURCES:uobject/%.c=obj/dbg/%.o)

build: build-dbg build-opt
build-dbg: lib/libuobject-dbg.a
build-opt: lib/libuobject.so lib/libuobject.a

-include obj/uobject.depend

obj/uobject.depend: $(wildcard uobject/*.[ch])
	@ mkdir -p $(shell dirname $@)
	@ (set -e; \
	   for SOURCE in $(SOURCES); \
	   do NAME=`basename $$SOURCE | sed 's/\.c$$//'`; \
	      $(CC) $(OPT_CPPFLAGS) -I. -MM -MT obj/opt/$$NAME.o -MT obj/opt/$$NAME.os $$SOURCE; \
	      $(CC) $(DBG_CPPFLAGS) -I. -MM -MT obj/dbg/$$NAME.o $$SOURCE; \
	   done) > $@

obj/opt/%.os: uobject/%.c
	@ mkdir -p $(shell dirname $@)
	$(CC) $(OPT_CPPFLAGS) -I. -DPIC $(OPT_CFLAGS) -fPIC -c -o $@ uobject/$*.c

obj/opt/%.o: uobject/%.c
	@ mkdir -p $(shell dirname $@)
	$(CC) $(OPT_CPPFLAGS) -I. $(OPT_CFLAGS) -c -o $@ uobject/$*.c

obj/dbg/%.o: uobject/%.c
	@ mkdir -p $(shell dirname $@)
	$(CC) $(DBG_CPPFLAGS) -I. $(DBG_CFLAGS) -c -o $@ uobject/$*.c

lib/libuobject.so: $(OPT_PIC_OBJECTS)
	@ mkdir -p $(shell dirname $@)
	$(CC) $(OPT_CFLAGS) -fPIC $(OPT_LDFLAGS) -shared -o $@ $(OPT_PIC_OBJECTS) -Wl,-soname,libuobject.so.$(SOVERSION)

lib/libuobject.a: $(OPT_OBJECTS)
	@ mkdir -p $(shell dirname $@)
	$(AR) cr $@ $(OPT_OBJECTS)
	$(RANLIB) $@

lib/libuobject-dbg.a: $(DBG_OBJECTS)
	@ mkdir -p $(shell dirname $@)
	$(AR) cr $@ $(DBG_OBJECTS)
	$(RANLIB) $@

install: build
	$(INSTALL) -d $(DESTDIR)$(PREFIX)/include/uobject
	$(INSTALL) -m 644 uobject/*.h $(DESTDIR)$(PREFIX)/include/uobject/

	$(INSTALL) -d $(DESTDIR)$(PREFIX)/lib
	$(INSTALL) -m 644 lib/libuobject-dbg.a $(DESTDIR)$(PREFIX)/lib/
	$(INSTALL) -m 644 lib/libuobject.a $(DESTDIR)$(PREFIX)/lib/
	$(INSTALL) -m 644 lib/libuobject.so $(DESTDIR)$(PREFIX)/lib/libuobject.so.$(SOVERSION)
	ln -sf libuobject.so.$(SOVERSION) $(DESTDIR)$(PREFIX)/lib/libuobject.so

TEST_SOURCES		= $(wildcard test/*.c)
TEST_DBG_PROGRAMS	= $(TEST_SOURCES:test/%.c=bin/%-dbg)
TEST_OPT_PROGRAMS	= $(TEST_SOURCES:test/%.c=bin/%-opt)

check-dbg: $(TEST_DBG_PROGRAMS)
check-opt: $(TEST_OPT_PROGRAMS)
check: check-dbg check-opt

$(TEST_DBG_PROGRAMS): lib/libuobject-dbg.a
$(TEST_OPT_PROGRAMS): lib/libuobject.a

-include obj/test.depend

obj/test.depend: $(wildcard test/*.[ch]) $(wildcard uobject/*.[ch])
	@ mkdir -p $(shell dirname $@)
	@ (set -e; \
	   for SOURCE in $(TEST_SOURCES); \
	   do NAME=`basename $$SOURCE | sed 's/\.c$$//'`; \
	      $(CC) $(OPT_CPPFLAGS) -I. -MM -MT bin/$$NAME-opt $$SOURCE; \
	      $(CC) $(DBG_CPPFLAGS) -I. -MM -MT bin/$$NAME-dbg $$SOURCE; \
	   done) > $@

bin/%-dbg: test/%.c
	@ mkdir -p $(shell dirname $@)
	$(CC) $(DBG_CPPFLAGS) -I. $(DBG_CFLAGS) -Wno-unused $(DBG_LDFLAGS) -o $@ test/$*.c lib/libuobject-dbg.a
	$@

bin/%-opt: test/%.c
	@ mkdir -p $(shell dirname $@)
	$(CC) $(OPT_CPPFLAGS) -I. $(OPT_CFLAGS) $(OPT_LDFLAGS) -o $@ test/$*.c lib/libuobject.a
	$@

doc:
	rm -rf html
	VERSION=$(VERSION) $(DOXYGEN)

clean:
	rm -rf bin lib obj html

.PHONY: build build-dbg build-opt install check check-dbg check-opt doc clean
