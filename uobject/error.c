/*
 * Copyright 2006  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

#include "error.h"
#include <stddef.h>
#include <assert.h>

void error_construct(struct error * this)
{
	assert(this);

	this->domain = NULL;
	this->reason = NULL;
	this->errno = 0;
}

void error_clear(struct error * this)
{
	assert(this);

	this->domain = NULL;
	this->reason = NULL;
	this->errno = 0;
}

void error_copy(struct error * this,
                struct error const * original)
{
	assert(this);
	assert(original);

	this->domain = original->domain;
	this->reason = original->reason;
	this->errno = original->errno;
}
