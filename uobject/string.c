/*
 * Copyright 2006  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

#include "string.h"
#include "memory.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#ifndef max
# define max(a, b) (((a) > (b)) ? (a) : (b))
#endif

#define STRING_CAPACITY_STEP  64

static
char * string_static_buffer(struct string const * this)
{
	if (this->is_static)
		return ((struct string_static *) this)->static_buffer;
	else
		return "";
}

static
char * string_active_buffer(struct string const * this)
{
	if (this->dynamic_buffer == NULL)
		return string_static_buffer(this);
	else
		return this->dynamic_buffer;
}

static
void string_realloc(struct string * this,
                    size_t length,
                    struct error * error)
{
	if (length > 0) {
		size_t new_capacity;
		char * new_buffer;

		new_capacity = length + 1;

		if (this->dynamic_buffer == NULL) {
			new_buffer = memory_alloc(new_capacity, error);
			error_return_if_set (error);

			memcpy(new_buffer, string_static_buffer(this), this->length + 1);
		} else {
			new_buffer = memory_realloc(this->dynamic_buffer, new_capacity, error);
			error_return_if_set (error);
		}

		this->capacity = new_capacity;
		this->dynamic_buffer = new_buffer;
	} else {
		if (this->is_static) {
			this->capacity = sizeof (((struct string_static *) this)->static_buffer);
			((struct string_static *) this)->static_buffer[0] = '\0';
		} else {
			this->capacity = 0;
		}

		if (this->dynamic_buffer) {
			memory_free(this->dynamic_buffer);
			this->dynamic_buffer = NULL;
		}
	}
}

/* string */

void string_construct(struct string * this)
{
	assert(this);

	this->length = 0;
	this->capacity = 1;
	this->dynamic_buffer = NULL;
	this->is_static = false;
}

void string_destruct(struct string const * this)
{
	assert(this);

	if (this->dynamic_buffer)
		memory_free(this->dynamic_buffer);
}

static
void string_construct_copy(struct string * this,
                           struct string const * original,
                           struct error * error)
{
	assert(this);
	assert(original);
	assert(error_not_set(error));

	this->length = original->length;

	if (this->length > 0) {
		this->capacity = this->length + 1;
		this->dynamic_buffer = memory_alloc(this->capacity, error);
		error_return_if_set (error);

		memcpy(this->dynamic_buffer, string_active_buffer(original), this->length + 1);
	} else {
		this->capacity = 0;
		this->dynamic_buffer = NULL;
	}

	this->is_static = false;
}

void string_clear(struct string * this)
{
	assert(this);

	if (this->is_static) {
		this->capacity = sizeof (((struct string_static *) this)->static_buffer);
		((struct string_static *) this)->static_buffer[0] = '\0';

		if (this->dynamic_buffer)
			memory_free(this->dynamic_buffer);

		this->dynamic_buffer = NULL;
	} else {
		if (this->dynamic_buffer)
			this->dynamic_buffer[0] = '\0';
	}

	this->length = 0;
}

int string_utf8_length(struct string const * this)
{
	assert(this);

	int length = 0;
	char * buffer;

	buffer = string_active_buffer(this);

	for (size_t i = 0; i < this->length; length++) {
		uint8_t byte;

		byte = buffer[i++];

		if ((byte & 0x80) != 0) {
			unsigned int skip;
			size_t next;

			if ((byte & 0xe0) == 0xc0)
				skip = 1;
			else if ((byte & 0xf0) == 0xe0)
				skip = 2;
			else if ((byte & 0xf8) == 0xf0)
				skip = 3;
			else
				return -1;

			next = i + skip;
			if (next > this->length)
				return -1;

			for (; i < next; i++) {
				byte = buffer[i];

				if ((byte & 0xc0) != 0x80)
					return -1;
			}
		}
	}

	return length;
}

char const * string_contents(struct string const * this)
{
	assert(this);

	return string_active_buffer(this);
}

void string_assign_string(struct string * this,
                          struct string const * other,
                          struct error * error)
{
	assert(this);
	assert(other);
	assert(error_not_set(error));

	string_assign_array(this, string_active_buffer(other), string_length(other), error);
}

void string_assign_array(struct string * this,
                         char const * data,
                         size_t length,
                         struct error * error)
{
	assert(this);
	assert(length == 0 || data != NULL);
	assert(error_not_set(error));

	char * buffer;

	if (length != this->capacity - 1) {
		string_realloc(this, length, error);
		error_return_if_set (error);
	}

	if (length > 0) {
		buffer = string_active_buffer(this);

		memcpy(buffer, data, length);
		buffer[length] = '\0';
	}

	this->length = length;
}

void string_insert_string(struct string * this,
                          size_t offset,
                          struct string const * other,
                          struct error * error)
{
	assert(other);
	assert(error_not_set(error));

	string_insert_array(this, offset, string_active_buffer(other), string_length(other), error);
}

void string_insert_array(struct string * this,
                         size_t offset,
                         char const * data,
                         size_t size,
                         struct error * error)
{
	assert(this);
	assert(offset <= this->length);
	assert(size == 0 || data != NULL);
	assert(error_not_set(error));

	size_t new_length;
	size_t alloc_length;
	char * buffer;

	if (size > 0) {
		new_length = this->length + size;

		if (new_length >= this->capacity) {
			alloc_length = this->length + max(STRING_CAPACITY_STEP, size);

			string_realloc(this, alloc_length, error);
			error_return_if_set (error);
		}

		buffer = string_active_buffer(this);

		memmove(buffer + offset + size, buffer + offset, this->length - offset);
		memcpy(buffer + offset, data, size);
		buffer[new_length] = '\0';

		this->length = new_length;
	}
}

void string_append_string(struct string * this,
                          struct string const * other,
                          struct error * error)
{
	assert(other);

	string_append_array(this, string_active_buffer(other), string_length(other), error);
}

void string_append_array(struct string * this,
                         char const * data,
                         size_t size,
                         struct error * error)
{
	string_insert_array(this, string_length(this), data, size, error);
}

OBJECT_INITIALIZE_CLASS(string,
                        string_construct,
                        string_destruct,
                        string_construct_copy,
                        string_assign_string);

/* string with static buffer */

void string_static_construct(struct string_static * this)
{
	assert(this);

	this->string.length = 0;
	this->string.capacity = sizeof (this->static_buffer);
	this->string.dynamic_buffer = NULL;
	this->string.is_static = true;

	this->static_buffer[0] = '\0';
}

static
void string_static_construct_copy(struct string_static * this,
                                  struct string const * original,
                                  struct error * error)
{
	assert(this);
	assert(original);
	assert(error_not_set(error));

	this->string.length = original->length;

	if (this->string.length > 0) {
		char * buffer;

		if (this->string.length >= sizeof (this->static_buffer)) {
			this->string.capacity = this->string.length + 1;
			this->string.dynamic_buffer = memory_alloc(this->string.capacity, error);
			error_return_if_set (error);

			buffer = this->string.dynamic_buffer;
		} else {
			this->string.capacity = sizeof (this->static_buffer);
			this->string.dynamic_buffer = NULL;

			buffer = this->static_buffer;
		}

		memcpy(buffer, string_active_buffer(original), this->string.length + 1);
	} else {
		this->string.capacity = 0;
		this->string.dynamic_buffer = NULL;

		this->static_buffer[0] = '\0';
	}

	this->string.is_static = true;
}

OBJECT_INITIALIZE_CLASS(string_static,
                        string_static_construct,
                        string_destruct,
                        string_static_construct_copy,
                        string_assign_string);
