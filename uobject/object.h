/*
 * Copyright 2006  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

/**
 * \defgroup object Object
 * \{
 *
 * \code
 * #include <uobject/object.h>
 * // Pollutes the object_* and error_* name spaces.
 * \endcode
 *
 * An object has a symbolic name.  It is given as the \c Name parameter to
 * macros which manipulate objects.  The object name is decoupled from the
 * associated data type:
 * - The primary purpose is to support arbitrary type naming conventions: the
 *   data type of object \c "example" could be \c "struct example" or \c
 *   "Example" depending on the user.
 * - This also means that the data type need not be a \c struct.
 * - As a side effect, it is possible to define multiple objects (with
 *   different \link object_semantics semantics\endlink) for a single data
 *   type.  (This should probably be seen as a bad thing.)
 *
 * \par Scoped instantiation example
 *
 * \code
 * #include <uobject/error.h>
 * #include <uobject/object.h>
 * #include <stddef.h>
 *
 * // Imaginary objects.
 * #include "file.h"
 * #include "buffer.h"
 *
 * void print_data(struct buffer *, struct error *);
 *
 * void print_file(char const * path, struct error * error)
 * {
 *         object_scoped_new(file, f);
 *         object_scoped_new(buffer, buf);
 *         size_t size;
 *
 *         file_open(f, path, error);
 *         error_goto_if_set (error, clean);
 *
 *         size = file_get_size(f, error);
 *         error_goto_if_set (error, clean);
 *
 *         buffer_init(buf, size, error);
 *         error_goto_if_set (error, clean);
 *
 *         file_read(f, buf, error);
 *         error_goto_if_set (error, clean);
 *
 *         print_data(buf, error);
 *
 * clean:
 *         object_scoped_delete(buf);
 *         object_scoped_delete(f);
 * }
 * \endcode
 *
 * \par Dynamic instantiation example
 *
 * The same thing using dynamic instances (for the sake of example):
 *
 * \code
 * void print_file(char const * path, struct error * error)
 * {
 *         struct file * f = NULL;
 *         struct buffer * buf = NULL;
 *         size_t size;
 *
 *         f = object_dynamic_new(file, error);
 *         error_goto_if_set (error, clean);
 *
 *         file_open(f, path, error);
 *         error_goto_if_set (error, clean);
 *
 *         size = file_get_size(f, error);
 *         error_goto_if_set (error, clean);
 *
 *         buf = object_dynamic_new(buffer, error);
 *         error_goto_if_set (error, clean);
 *
 *         buffer_init(buf, size, error);
 *         error_goto_if_set (error, clean);
 *
 *         file_read(f, buf, error);
 *         error_goto_if_set (error, clean);
 *
 *         print_data(buf, error);
 *
 * clean:
 *         object_dynamic_delete(buffer, buf);
 *         object_dynamic_delete(file, f);
 * }
 * \endcode
 *
 * \par Example object implementation
 *
 * \c example.h
 *
 * \code
 * #include <uobject/object.h>
 * #include <uobject/error.h>
 *
 * struct example;
 *
 * // Constructor and destructor need to be seen by object_scoped_new().
 * void example_construct(struct example * this);
 * void example_destruct(struct example const * this);
 *
 * // Copy method might be generally useful.
 * void example_copy(struct example * this,
 *                   struct example const * original,
 *                   struct error *);
 *
 * OBJECT_DEFINE_TYPE(example, struct example);
 * OBJECT_DECLARE_CLASS(example);
 * \endcode
 *
 * \c example.c
 *
 * \code
 * #include "example.h"
 *
 * // Copy constructor is only accessed through the object_class.
 * static void example_construct_copy(struct example * this,
 *                                    struct example const * original,
 *                                    struct error *);
 *
 * OBJECT_INITIALIZE_COPYABLE_CLASS(example);
 * \endcode
 *
 * \todo \c object_compare_method_t for natural/default ordering.
 * \todo \c object_custom_constructor_t and the associated instantiation API.
 */

#ifndef UOBJECT_OBJECT_H
#define UOBJECT_OBJECT_H

#include "error.h"
#include <stddef.h>
#include <stdbool.h>

/**
 * \defgroup object_semantics Object semantics
 * \{
 */

/**
 * Mutable instance access.
 */
typedef void * object_instance_t;

/**
 * Immutable instance access.
 */
typedef void const * object_const_instance_t;

/**
 * Initialize \a this to its default state.  The implementation must be
 * trivial enough to always succeed, because error checking is not supported.
 * Any resource allocation must be done afterwards.
 *
 * \param this points to uninitialized memory which has been reserved for the
 *             instance.
 *
 * \post \a this is an instance with a defined state.
 */
typedef void (* object_constructor_t)(object_instance_t this);

/**
 * Release all resources owned by \a this.
 *
 * \param this points to an initialized instance.
 *
 * \post \a this points to an uninitialized memory region.
 */
typedef void (* object_destructor_t)(object_const_instance_t this);

/**
 * Initialize \a this by duplicating the state of \a original.
 *
 * \param error is an unset ::error instance.
 *
 * \post \a this is equivalent to \a original, or \a error is set and the
 *       contents of the memory pointed to by \a this are undefined (an
 *       instance has not been constructed).
 */
typedef void (* object_copy_constructor_t)(object_instance_t this,
                                           object_const_instance_t original,
                                           struct error * error);

/**
 * Duplicate the state of \a original by replacing the state of \a this.
 *
 * \param this points to an initialized instance.
 * \param error is an unset ::error instance.
 *
 * \post \a this is equivalent to \a original, or \a error is set and \a this is
 *       unchanged.
 */
typedef void (* object_copy_method_t)(object_instance_t this,
                                      object_const_instance_t original,
                                      struct error * error);

/**
 * Indirect object type information.  Needed for \link object_dynamic
 * dynamic\endlink and \link vector_object vector-based\endlink instance
 * management algorithms.
 */
struct object_class
{
	size_t size;
	object_constructor_t construct;
	object_destructor_t destruct;
	object_copy_constructor_t construct_copy;
	object_copy_method_t copy;
};

/**
 * \}
 */

/**
 * \defgroup object_virtual Virtual base
 * \{
 */

/**
 * The virtual method implementations of an object_virtual_base.
 */
struct object_virtual_table
{
	/**
	 * The real destructor.
	 */
	object_destructor_t destruct;
};

/**
 * Abstract base for virtual objects.
 */
struct object_virtual_base
{
	/**
	 * Virtual method implementations.  May be a derivative of
	 * object_virtual_table which contains additional object-specific
	 * methods.
	 */
	struct object_virtual_table const * table;
};

/**
 * Mutable virtual instance access.
 */
typedef struct object_virtual_base * object_virtual_instance_t;

void object_virtual_destruct(struct object_virtual_base const *);

/**
 * \}
 */

/**
 * \defgroup object_scoped Scoped instantiation
 * \{
 *
 * Instances are allocated on the stack.
 */

/**
 * Allocate and construct an instance on the stack.  The created instance
 * must be destroyed with object_scoped_delete() before control exits the
 * calling scope.
 *
 * \param Name
 * \param InstanceName is an unused variable name.
 *
 * \post The \a InstanceName pointer is declared in the calling scope.
 */
#define object_scoped_new(Name, InstanceName) \
	Name##_object_t InstanceName##_object_storage; \
	Name##_object_t * const InstanceName = &InstanceName##_object_storage; \
	void (* const InstanceName##_object_destruct)(Name##_object_t const *) \
		= Name##_destruct; \
	Name##_construct(InstanceName)

/**
 * Destroy a stack-allocated instance.
 *
 * \param Instance is a pointer to the target instance.
 */
#define object_scoped_delete(Instance) \
	Instance##_object_destruct(Instance)

/**
 * \}
 */

/**
 * \defgroup object_dynamic Dynamic instantiation
 * \{
 *
 * Instances are allocated from the \link memory heap\endlink.
 */

object_instance_t object_dynamic_class_new(struct object_class const *,
                                           struct error *);
object_instance_t object_dynamic_class_new_copy(struct object_class const *,
                                                object_const_instance_t,
                                                struct error *);
void object_dynamic_class_delete(struct object_class const *,
                                 object_instance_t);
void object_dynamic_delete_virtual(object_virtual_instance_t);

/**
 * Allocate and construct an instance on the heap.
 *
 * \param Name
 * \param Error is a pointer to an unset ::error instance to be set on
 *              failure.
 *
 * \return Pointer to the instance, or \c NULL if \a Error was set.
 */
#define object_dynamic_new(Name, Error) \
	((Name##_object_t *) object_dynamic_class_new( \
		&Name##_object_class, \
		Error))

/**
 * Allocate and construct an instance on the heap that is a copy of an
 * existing instance.
 *
 * \param Name
 * \param Original is a pointer to the instance to be copied.
 * \param Error is a pointer to an unset ::error instance to be set on
 *              failure.
 *
 * \return Pointer to the instance, or \c NULL if \a Error was set.
 */
#define object_dynamic_new_copy(Name, Original, Error) \
	((Name##_object_t *) object_dynamic_class_new_copy( \
		&Name##_object_class, \
		Original, \
		Error))

/**
 * Destroy and free a heap-allocated object instance.
 *
 * \param Name
 * \param Instance is a pointer to a \a Name instance (may be \c NULL).
 */
#define object_dynamic_delete(Name, Instance) \
	object_dynamic_class_delete( \
		&Name##_object_class, \
		Instance)

/**
 * Destroy and free a heap-allocated instance of an object derived from
 * object_virtual_base.
 *
 * \param Instance is a ::object_virtual_instance_t (may be \c NULL).
 */
/* This macro is here to produce consistent-looking API documentation. */
#define object_dynamic_delete_virtual(Instance) \
	object_dynamic_delete_virtual(Instance)

/**
 * \}
 */

/**
 * \defgroup object_implementation Object implementation
 * \{
 */

/**
 * Expands to an object's data type name.
 */
#define OBJECT_TYPE_NAME(Name) \
	Name##_object_t

/**
 * Expands to an object's class name.
 */
#define OBJECT_CLASS_NAME(Name) \
	Name##_object_class

/**
 * Define the data type of an object.
 */
#define OBJECT_DEFINE_TYPE(Name, Type) \
	typedef Type Name##_object_t

/**
 * Declare the object_class of an object.
 */
#define OBJECT_DECLARE_CLASS(Name) \
	extern struct object_class const Name##_object_class

/**
 * Create an object_class instance.
 *
 * \param Name
 * \param Constructor is a ::object_constructor_t.
 * \param Destructor is a ::object_destructor_t.
 * \param CopyConstructor is a ::object_copy_constructor_t.
 * \param CopyMethod is a ::object_copy_method_t.
 */
#define OBJECT_INITIALIZE_CLASS(Name, Constructor, Destructor, \
                                CopyConstructor, CopyMethod) \
	struct object_class const Name##_object_class = { \
		sizeof (Name##_object_t), \
		(object_constructor_t) Constructor, \
		(object_destructor_t) Destructor, \
		(object_copy_constructor_t) CopyConstructor, \
		(object_copy_method_t) CopyMethod, \
	}

/**
 * Create an object_class instance with defaults.
 */
#define OBJECT_INITIALIZE_COPYABLE_CLASS(Name) \
	struct object_class const Name##_object_class = { \
		sizeof (Name##_object_t), \
		(object_constructor_t) Name##_construct, \
		(object_destructor_t) Name##_destruct, \
		(object_copy_constructor_t) Name##_construct_copy, \
		(object_copy_method_t) Name##_copy, \
	}

/**
 * Create an object_class instance with default constructor and destructor.
 */
#define OBJECT_INITIALIZE_NONCOPYABLE_CLASS(Name) \
	struct object_class const Name##_object_class = { \
		sizeof (Name##_object_t), \
		(object_constructor_t) Name##_construct, \
		(object_destructor_t) Name##_destruct, \
		NULL, \
		NULL, \
	}

/**
 * Create an object_class instance for an object derived from
 * object_virtual_base.  The virtual destructor is used as the destructor.
 *
 * \param Name
 * \param Constructor is a ::object_constructor_t.
 * \param CopyConstructor is a ::object_copy_constructor_t.
 * \param CopyMethod is a ::object_copy_method_t.
 */
#define OBJECT_INITIALIZE_VIRTUAL_CLASS(Name, Constructor, \
                                        CopyConstructor, CopyMethod) \
	struct object_class const Name##_object_class = { \
		sizeof (Name##_object_t), \
		(object_constructor_t) Constructor, \
		(object_destructor_t) object_virtual_destruct, \
		(object_copy_constructor_t) CopyConstructor, \
		(object_copy_method_t) CopyMethod, \
	}

/**
 * Create an object_class instance for an object derived from
 * object_virtual_base.  The virtual destructor is used as the destructor;
 * defaults are used otherwise.
 */
#define OBJECT_INITIALIZE_COPYABLE_VIRTUAL_CLASS(Name) \
	struct object_class const Name##_object_class = { \
		sizeof (Name##_object_t), \
		(object_constructor_t) Name##_construct, \
		(object_destructor_t) object_virtual_destruct, \
		(object_copy_constructor_t) Name##_construct_copy, \
		(object_copy_method_t) Name##_copy, \
	}

/**
 * Create an object_class instance for an object derived from
 * object_virtual_base.  Default constructor and virtual destructor are used.
 */
#define OBJECT_INITIALIZE_NONCOPYABLE_VIRTUAL_CLASS(Name) \
	struct object_class const Name##_object_class = { \
		sizeof (Name##_object_t), \
		(object_constructor_t) Name##_construct, \
		(object_destructor_t) object_virtual_destruct, \
		NULL, \
		NULL, \
	}

/**
 * \}
 */

#endif

/**
 * \}
 */
