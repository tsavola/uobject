/*
 * Copyright 2006  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

#include "vector.h"
#include "memory.h"
#include "object.h"
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#ifndef min
# define min(a, b) (((a) < (b)) ? (a) : (b))
#endif

#define VECTOR_CAPACITY_STEP  16

static
void vector_data_realloc(struct vector_data * this,
                         size_t capacity,
                         struct error * error)
{
	void * buffer;

	if (capacity > 0) {
		if (this->buffer)
			buffer = memory_realloc(this->buffer, capacity, error);
		else
			buffer = memory_alloc(capacity, error);

		error_return_if_set (error);
	} else {
		if (this->buffer)
			memory_free(this->buffer);

		buffer = NULL;
	}

	this->capacity = capacity;
	this->buffer = buffer;
}

/* data vector */

void vector_data_construct(struct vector_data * this)
{
	assert(this);

	this->size = 0;
	this->capacity = 0;
	this->buffer = NULL;
}

void vector_data_destruct(struct vector_data const * this)
{
	assert(this);

	if (this->buffer)
		memory_free(this->buffer);
}

static
void vector_data_construct_copy(struct vector_data * this,
                                struct vector_data const * original,
                                struct error * error)
{
	assert(this);
	assert(original);
	assert(error_not_set(error));

	this->size = original->size;
	this->capacity = this->size;

	if (this->size > 0) {
		this->buffer = memory_alloc(this->size, error);
		error_return_if_set (error);

		memcpy(this->buffer, original->buffer, this->size);
	} else {
		this->buffer = NULL;
	}
}

void vector_data_copy(struct vector_data * this,
                      struct vector_data const * original,
                      struct error * error)
{
	assert(this);
	assert(original);
	assert(error_not_set(error));

	if (original->size != this->capacity) {
		vector_data_realloc(this, original->size, error);
		error_return_if_set (error);
	}

	memcpy(this->buffer, original->buffer, original->size);
	this->size = original->size;
}

void vector_data_reserve(struct vector_data * this,
                         size_t capacity,
                         struct error * error)
{
	assert(this);
	assert(error_not_set(error));

	if (capacity > this->capacity)
		vector_data_realloc(this, capacity, error);
}

void * vector_data_push(struct vector_data * this,
                        void const * data,
                        size_t data_size,
                        struct error * error)
{
	return vector_data_insert(this, vector_data_size(this), data, data_size, error);
}

void vector_data_pop(struct vector_data * this,
                     void * data,
                     size_t data_size)
{
	vector_data_remove(this, vector_data_size(this) - data_size, data, data_size);
}

void * vector_data_insert(struct vector_data * this,
                          size_t offset,
                          void const * data,
                          size_t data_size,
                          struct error * error)
{
	assert(this);
	assert(offset <= this->size);
	assert(error_not_set(error));

	size_t new_size;
	size_t new_capacity;

	new_size = this->size + data_size;

	if (new_size > this->capacity) {
		new_capacity = new_size + (VECTOR_CAPACITY_STEP - 1) * data_size;

		vector_data_realloc(this, new_capacity, error);
		error_return_value_if_set (error, NULL);
	}

	memmove(this->buffer + offset + data_size, this->buffer + offset, this->size - offset);
	this->size = new_size;

	if (data)
		memcpy(this->buffer + offset, data, data_size);

	return this->buffer + offset;
}

void vector_data_remove(struct vector_data * this,
                        size_t offset,
                        void * data,
                        size_t data_size)
{
	assert(this);
	assert(offset + data_size <= this->size);

	if (data)
		memcpy(data, this->buffer + offset, data_size);

	this->size -= data_size;
	memmove(this->buffer + offset, this->buffer + offset + data_size, this->size - offset);
}

void vector_data_sort(struct vector_data * this,
                      vector_data_comparison_t compare,
                      size_t data_size)
{
	assert(this);
	assert(compare);
	assert(data_size > 0);
	assert(this->size % data_size == 0);

	qsort(this->buffer, this->size / data_size, data_size, compare);
}

/* pointer vector */

OBJECT_INITIALIZE_CLASS(vector_pointer,
                        vector_data_construct,
                        vector_data_destruct,
                        vector_data_construct_copy,
                        vector_data_copy);

/* value vector */

OBJECT_INITIALIZE_CLASS(vector_value,
                        vector_data_construct,
                        vector_data_destruct,
                        vector_data_construct_copy,
                        vector_data_copy);

/* object vector */

void vector_object_construct(struct vector_object * this)
{
	assert(this);

	vector_data_construct(&this->data);
	this->object = NULL;
}

void vector_object_destruct(struct vector_object const * this)
{
	assert(this);

	object_instance_t instance;

	if (this->object)
		for (size_t i = 0; i < this->data.size; i += this->object->size) {
			instance = this->data.buffer + i;
			this->object->destruct(instance);
		}

	vector_data_destruct(&this->data);
}

static
void vector_object_construct_copy(struct vector_object * this,
                                  struct vector_object const * original,
                                  struct error * error)
{
	assert(this);
	assert(original);
	assert(original->object);
	assert(error_not_set(error));

	vector_data_construct(&this->data);
	this->object = original->object;
	vector_object_copy(this, original, error);
}

void vector_object_copy(struct vector_object * this,
                        struct vector_object const * original,
                        struct error * error)
{
	assert(this);
	assert(original);
	assert(original->object);
	assert(this->object->construct_copy);
	assert(this->object->copy);
	assert(error_not_set(error));

	if (this->object)
		assert(this->object == original->object);
	else
		this->object = original->object;

	if (original->data.size > this->data.capacity) {
		vector_data_realloc(&this->data, original->data.size, error);
		error_return_if_set (error);
	}

	for (size_t i = 0; i < min(this->data.size, original->data.size); i += this->object->size) {
		object_instance_t old_instance;
		object_const_instance_t original_instance;

		old_instance = this->data.buffer + i;
		original_instance = original->data.buffer + i;

		this->object->copy(old_instance, original_instance, error);
		error_return_if_set (error);
	}

	for (; this->data.size < original->data.size; this->data.size += this->object->size) {
		object_instance_t new_instance;
		object_const_instance_t original_instance;

		new_instance = this->data.buffer + this->data.size;
		original_instance = original->data.buffer + this->data.size;

		this->object->construct_copy(new_instance, original_instance, error);
		error_return_if_set (error);
	}

	for (size_t i = original->data.size; i < this->data.size; i += this->object->size) {
		object_instance_t old_instance;

		old_instance = this->data.buffer + i;

		this->object->destruct(old_instance);
	}

	this->data.size = original->data.size;

	if (this->data.size < this->data.capacity)
		vector_data_realloc(&this->data, this->data.size, error);
}

unsigned int vector_object_size(struct vector_object const * this)
{
	assert(this);
	assert(this->object);

	return this->data.size / this->object->size;
}

void * vector_object_index(struct vector_object const * this,
                           unsigned int index)
{
	assert(this);
	assert(this->object);

	size_t offset;

	offset = index * this->object->size;
	assert(offset < this->data.size);

	return this->data.buffer + offset;
}

void vector_object_reserve(struct vector_object * this,
                           unsigned int count,
                           struct error * error)
{
	assert(this);
	assert(this->object);
	assert(error_not_set(error));

	size_t capacity;

	capacity = count * this->object->size;
	vector_data_reserve(&this->data, capacity, error);
}

void vector_object_clear(struct vector_object * this)
{
	assert(this);
	assert(this->object);

	object_instance_t instance;

	for (size_t i = 0; i < this->data.size; i += this->object->size) {
		instance = this->data.buffer + i;
		this->object->destruct(instance);
	}

	this->data.size = 0;
}

object_instance_t vector_object_push(struct vector_object * this,
                                     struct error * error)
{
	assert(this);

	/** \todo Replace the size -> index -> offset conversion. */
	return vector_object_insert(this, this->data.size / this->object->size, error);
}

object_instance_t vector_object_push_copy(struct vector_object * this,
                                          object_const_instance_t original,
                                          struct error * error)
{
	assert(this);

	/** \todo Replace the size -> index -> offset conversion. */
	return vector_object_insert_copy(this, this->data.size / this->object->size, original, error);
}

void vector_object_pop(struct vector_object * this)
{
	assert(this);

	/** \todo Replace the size -> index -> offset conversion. */
	vector_object_remove(this, this->data.size / this->object->size - 1);
}

object_instance_t vector_object_insert(struct vector_object * this,
                                       unsigned int index,
                                       struct error * error)
{
	assert(this);
	assert(this->object);
	assert(error_not_set(error));

	size_t offset;
	object_instance_t instance;

	offset = index * this->object->size;
	assert(offset <= this->data.size);

	instance = vector_data_insert(&this->data, offset, NULL, this->object->size, error);
	if (error_not_set(error))
		this->object->construct(instance);

	return instance;
}

object_instance_t vector_object_insert_copy(struct vector_object * this,
                                            unsigned int index,
                                            object_const_instance_t original,
                                            struct error * error)
{
	assert(this);
	assert(original);
	assert(this->object);
	assert(this->object->construct_copy);
	assert(error_not_set(error));

	size_t offset;
	object_instance_t instance;

	offset = index * this->object->size;
	assert(offset <= this->data.size);

	instance = vector_data_insert(&this->data, offset, NULL, this->object->size, error);

	if (error_not_set(error)) {
		this->object->construct_copy(instance, original, error);

		if (error_is_set(error)) {
			vector_data_remove(&this->data, offset, NULL, this->object->size);
			return NULL;
		}
	}

	return instance;
}

void vector_object_remove(struct vector_object * this,
                          unsigned int index)
{
	assert(this);
	assert(this->object);

	size_t offset;
	object_instance_t instance;

	offset = index * this->object->size;
	assert(offset < this->data.size);

	instance = this->data.buffer + offset;
	this->object->destruct(instance);

	vector_data_remove(&this->data, offset, NULL, this->object->size);
}

void vector_object_sort(struct vector_object * this,
                        vector_object_comparison_t compare)
{
	assert(this);
	assert(this->object);

	vector_data_sort(&this->data, (vector_data_comparison_t) compare, this->object->size);
}

OBJECT_INITIALIZE_COPYABLE_CLASS(vector_object);
