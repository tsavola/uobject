/*
 * Copyright 2006  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

#include "memory.h"
#include <stdlib.h>
#include <errno.h>
#include <assert.h>

void * memory_alloc(size_t size, struct error * error)
{
	void * memory;

	assert(size > 0);
	assert(error_not_set(error));

	memory = malloc(size);
	if (memory == NULL)
		error_set(error, memory_error, memory_error_alloc, errno);

	return memory;
}

void * memory_realloc(void * memory, size_t size, struct error * error)
{
	assert(memory);
	assert(size > 0);
	assert(error_not_set(error));

	memory = realloc(memory, size);
	if (memory == NULL)
		error_set(error, memory_error, memory_error_alloc, errno);

	return memory;
}

error_domain_t const memory_error = "Memory";
error_reason_t const memory_error_alloc = "Allocation";
