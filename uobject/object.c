/*
 * Copyright 2006  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

#include "object.h"
#include "memory.h"
#include <assert.h>

void object_virtual_destruct(struct object_virtual_base const * this)
{
	assert(this);
	assert(this->table);
	assert(this->table->destruct);

	this->table->destruct(this);
}

object_instance_t object_dynamic_class_new(struct object_class const * object,
                                           struct error * error)
{
	assert(object);
	assert(object->size > 0);
	assert(object->construct);
	assert(error_not_set(error));

	object_instance_t instance;

	instance = memory_alloc(object->size, error);

	if (error_not_set(error))
		object->construct(instance);

	return instance;
}

object_instance_t object_dynamic_class_new_copy(struct object_class const * object,
                                                object_const_instance_t original,
                                                struct error * error)
{
	assert(object);
	assert(object->size > 0);
	assert(object->construct_copy);
	assert(error_not_set(error));

	object_instance_t instance;

	instance = memory_alloc(object->size, error);

	if (error_not_set(error)) {
		object->construct_copy(instance, original, error);

		if (error_is_set(error)) {
			memory_free(instance);
			return NULL;
		}
	}

	return instance;
}

void object_dynamic_class_delete(struct object_class const * object,
                                 object_instance_t instance)
{
	assert(object);
	assert(object->destruct);

	if (instance) {
		object->destruct(instance);
		memory_free(instance);
	}
}

void object_dynamic_delete_virtual(struct object_virtual_base * instance)
{
	if (instance) {
		assert(instance->table);
		assert(instance->table->destruct);

		instance->table->destruct(instance);
		memory_free(instance);
	}
}
