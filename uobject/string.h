/*
 * Copyright 2006  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

/**
 * \defgroup string String
 * \{
 *
 * \code
 * #include <uobject/string.h>
 * // Pollutes the string_*, object_* and error_* name spaces.
 * \endcode
 *
 * Dynamic character string buffers.
 *
 * \par Static String example
 *
 * The following code does not cause any heap allocations for reasonably
 * sized paths, and the error checking is only necessary for corner cases:
 *
 * \code
 * #include <uobject/string.h>
 * #include <stdio.h>
 *
 * void print_filename(char const * location, char const * name, char const * suffix, struct error * error)
 * {
 *         object_scoped_new(string_static, path);
 *
 *         string_static_assign(path, location, error);
 *         error_goto_if_set (error, end);
 *
 *         string_static_append_char(path, '/', error);
 *         error_goto_if_set (error, end);
 *
 *         string_static_append(path, name, error);
 *         error_goto_if_set (error, end);
 *
 *         string_static_append_char(path, '.', error);
 *         error_goto_if_set (error, end);
 *
 *         string_static_append(path, suffix, error);
 *         error_goto_if_set (error, end);
 *
 *         printf("%s\n", string_static_contents(path));
 *
 * end:
 *         object_scoped_delete(path);
 * }
 * \endcode
 *
 * \todo \c string_reserve().
 */

#ifndef UOBJECT_STRING_H
#define UOBJECT_STRING_H

#include "error.h"
#include "object.h"
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

/**
 * \defgroup string_object String object
 * \{
 */

/**
 * String data type.
 */
struct string
{
	size_t length;
	size_t capacity;
	char * dynamic_buffer;
	bool is_static;
};

void string_construct(struct string *);
void string_destruct(struct string const *);

/**
 * Clear the contents of \a this.
 */
void string_clear(struct string * this);

/**
 * The string length.
 *
 * \return Number of characters.
 */
static inline
size_t string_length(struct string const * this)
{
	assert(this);

	return this->length;
}

/**
 * The UTF-8 string length.
 *
 * \return Number of UTF-8 characters.
 * \retval -1 if \a this does not contain a valid UTF-8 string.
 */
int string_utf8_length(struct string const * this);

/**
 * The C-string contained by \a this.
 *
 * \return Null-terminated read-only string.
 */
char const * string_contents(struct string const * this);

/**
 * Replace the contents of \a this with the contents of another string.
 *
 * \param this
 * \param other
 * \param error is an unset \link error_object Error\endlink to be set on
 *              failure.
 */
void string_assign_string(struct string * this,
                          struct string const * other,
                          struct error * error);

/**
 * Replace the contents of \a this with the contents of an array.
 *
 * \param this
 * \param data is the array to be copied.
 * \param size is the length of the array.
 * \param error is an unset \link error_object Error\endlink to be set on
 *              failure.
 */
void string_assign_array(struct string * this,
                         char const * data,
                         size_t size,
                         struct error * error);

/**
 * Replace the contents of \a this with a single character.
 *
 * \param this
 * \param character
 * \param error is an unset \link error_object Error\endlink to be set on
 *              failure.
 */
static inline
void string_assign_char(struct string * this,
                        char character,
                        struct error * error)
{
	string_assign_array(this, &character, 1, error);
}

/**
 * Replace the contents of \a this with the contents of a C-string.
 *
 * \param this
 * \param str is a null-terminated string.
 * \param error is an unset \link error_object Error\endlink to be set on
 *              failure.
 */
static inline
void string_assign(struct string * this,
                   char const * str,
                   struct error * error)
{
	assert(str);

	string_assign_array(this, str, strlen(str), error);
}

/**
 * Insert the contents of another string at position \a offset.
 *
 * \param this
 * \param offset
 * \param other
 * \param error is an unset \link error_object Error\endlink to be set on
 *              failure.
 */
void string_insert_string(struct string * this,
                          size_t offset,
                          struct string const * other,
                          struct error * error);

/**
 * Insert the contents of an array at position \a offset.
 *
 * \param this
 * \param offset
 * \param data is the array.
 * \param size is the length of the array.
 * \param error is an unset \link error_object Error\endlink to be set on
 *              failure.
 */
void string_insert_array(struct string * this,
                         size_t offset,
                         char const * data,
                         size_t size,
                         struct error * error);

/**
 * Insert a single character at position \a offset.
 *
 * \param this
 * \param offset
 * \param character
 * \param error is an unset \link error_object Error\endlink to be set on
 *              failure.
 */
static inline
void string_insert_char(struct string * this,
                        size_t offset,
                        char character,
                        struct error * error)
{
	string_insert_array(this, offset, &character, 1, error);
}

/**
 * Insert the contents of a C-string at position \a offset.
 *
 * \param this
 * \param offset
 * \param str is a null-terminated string.
 * \param error is an unset \link error_object Error\endlink to be set on
 *              failure.
 */
static inline
void string_insert(struct string * this,
                   size_t offset,
                   char const * str,
                   struct error * error)
{
	assert(str);

	string_insert_array(this, offset, str, strlen(str), error);
}

/**
 * Extend \a this with the contents of another string.
 *
 * \param this
 * \param other
 * \param error is an unset \link error_object Error\endlink to be set on
 *              failure.
 */
void string_append_string(struct string * this,
                          struct string const * other,
                          struct error * error);

/**
 * Extend \a this with the contents of an array.
 *
 * \param this
 * \param data is the array.
 * \param size is the length of the array.
 * \param error is an unset \link error_object Error\endlink to be set on
 *              failure.
 */
void string_append_array(struct string * this,
                         char const * data,
                         size_t size,
                         struct error * error);

/**
 * Extend \a this with a single character.
 *
 * \param this
 * \param character
 * \param error is an unset \link error_object Error\endlink to be set on
 *              failure.
 */
static inline
void string_append_char(struct string * this,
                        char character,
                        struct error * error)
{
	string_append_array(this, &character, 1, error);
}

/**
 * Extend \a this with the contents of a C-string.
 *
 * \param this
 * \param str is a null-terminated string.
 * \param error is an unset \link error_object Error\endlink to be set on
 *              failure.
 */
static inline
void string_append(struct string * this,
                   char const * str,
                   struct error * error)
{
	assert(str);

	string_append_array(this, str, strlen(str), error);
}

OBJECT_DEFINE_TYPE(string, struct string);
OBJECT_DECLARE_CLASS(string);

/**
 * \}
 */

/**
 * \defgroup string_static_object Static String object
 * \{
 *
 * A child of the \link string_object String\endlink object for fast
 * stack-allocation.  Uses dynamic allocation when the length grows over the
 * static capacity.
 */

/**
 * The initial capacity.
 */
#define STRING_STATIC_CAPACITY 4096

/**
 * Static String data type.
 */
struct string_static
{
	struct string string;
	char static_buffer[STRING_STATIC_CAPACITY];
};

void string_static_construct(struct string_static *);

static inline
void string_static_destruct(struct string_static const * this)
{
	string_destruct(&this->string);
}

/** \copydoc string_clear() */
static inline
void string_static_clear(struct string_static * this)
{
	string_clear(&this->string);
}

/** \copydoc string_length() */
static inline
size_t string_static_length(struct string_static const * this)
{
	return string_length(&this->string);
}

/** \copydoc string_utf8_length() */
static inline
int string_static_utf8_length(struct string_static const * this)
{
	return string_utf8_length(&this->string);
}

/** \copydoc string_contents() */
static inline
char const * string_static_contents(struct string_static const * this)
{
	return string_contents(&this->string);
}

/** \copydoc string_assign_string() */
static inline
void string_static_assign_static(struct string_static * this,
                                 struct string_static const * other,
                                 struct error * error)
{
	string_assign_string(&this->string, &other->string, error);
}

/** \copydoc string_assign_string() */
static inline
void string_static_assign_string(struct string_static * this,
                                 struct string const * other,
                                 struct error * error)
{
	string_assign_string(&this->string, other, error);
}

/** \copydoc string_assign_array() */
static inline
void string_static_assign_array(struct string_static * this,
                                char const * data,
                                size_t size,
                                struct error * error)
{
	string_assign_array(&this->string, data, size, error);
}

/** \copydoc string_assign_char() */
static inline
void string_static_assign_char(struct string_static * this,
                               char character,
                               struct error * error)
{
	string_assign_char(&this->string, character, error);
}

/** \copydoc string_assign() */
static inline
void string_static_assign(struct string_static * this,
                          char const * str,
                          struct error * error)
{
	string_assign(&this->string, str, error);
}

/** \copydoc string_insert_string() */
static inline
void string_static_insert_static(struct string_static * this,
                                 size_t offset,
                                 struct string_static const * other,
                                 struct error * error)
{
	string_insert_string(&this->string, offset, &other->string, error);
}

/** \copydoc string_insert_string() */
static inline
void string_static_insert_string(struct string_static * this,
                                 size_t offset,
                                 struct string const * other,
                                 struct error * error)
{
	string_insert_string(&this->string, offset, other, error);
}

/** \copydoc string_insert_array() */
static inline
void string_static_insert_array(struct string_static * this,
                                size_t offset,
                                char const * data,
                                size_t size,
                                struct error * error)
{
	string_insert_array(&this->string, offset, data, size, error);
}

/** \copydoc string_insert_char() */
static inline
void string_static_insert_char(struct string_static * this,
                               size_t offset,
                               char character,
                               struct error * error)
{
	string_insert_char(&this->string, offset, character, error);
}

/** \copydoc string_insert() */
static inline
void string_static_insert(struct string_static * this,
                          size_t offset,
                          char const * str,
                          struct error * error)
{
	string_insert(&this->string, offset, str, error);
}

/** \copydoc string_append_string() */
static inline
void string_static_append_static(struct string_static * this,
                                 struct string_static const * other,
                                 struct error * error)
{
	string_append_string(&this->string, &other->string, error);
}

/** \copydoc string_append_string() */
static inline
void string_static_append_string(struct string_static * this,
                                 struct string const * other,
                                 struct error * error)
{
	string_append_string(&this->string, other, error);
}

/** \copydoc string_append_array() */
static inline
void string_static_append_array(struct string_static * this,
                                char const * data,
                                size_t size,
                                struct error * error)
{
	string_append_array(&this->string, data, size, error);
}

/** \copydoc string_append_char() */
static inline
void string_static_append_char(struct string_static * this,
                               char character,
                               struct error * error)
{
	string_append_char(&this->string, character, error);
}

/** \copydoc string_append() */
static inline
void string_static_append(struct string_static * this,
                          char const * str,
                          struct error * error)
{
	string_append(&this->string, str, error);
}

OBJECT_DEFINE_TYPE(string_static, struct string_static);
OBJECT_DECLARE_CLASS(string_static);

/**
 * \}
 */

#endif

/**
 * \}
 */
