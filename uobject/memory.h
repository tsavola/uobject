/*
 * Copyright 2006  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

/**
 * \defgroup memory Memory
 * \{
 *
 * \code
 * #include <uobject/memory.h>
 * // Pollutes the memory_* and error_* name spaces.
 * \endcode
 *
 * A layer on top of standard heap memory management functions with
 * \link error error handling\endlink support.
 */

#ifndef UOBJECT_MEMORY_H
#define UOBJECT_MEMORY_H

#include "error.h"
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#ifdef __GNUC__
# define MEMORY_ATTRIBUTE_MALLOC __attribute__ ((malloc))
#else
# define MEMORY_ATTRIBUTE_MALLOC
#endif

/**
 * Allocate memory.  The contents will be uninitialized.
 *
 * \param size in bytes.
 * \param error is an unset \link error_object Error\endlink to be set on
 *              failure.
 *
 * \return Pointer to a valid memory region, or \c NULL if \a error was set.
 */
void * memory_alloc(size_t size,
                    struct error * error) MEMORY_ATTRIBUTE_MALLOC;

/**
 * Resize a memory region.  The contents of the old part will be preserved;
 * contents of any new part will be uninitialized.
 *
 * \param region points to memory allocated with memory_alloc() or this
 *               function.
 * \param size in bytes.
 * \param error is an unset \link error_object Error\endlink to be set on
 *              failure.
 *
 * \return Pointer to a valid memory region, or \c NULL if \a error was set.
 *
 * \post \a region points to an invalid memory region if its value differs
 *       from the returned pointer.
 */
void * memory_realloc(void * region,
                      size_t size,
                      struct error * error);

/**
 * Free a memory region.
 *
 * \param region is a pointer returned from memory_alloc() or
 *               memory_realloc().
 *
 * \post \a region points to an invalid memory region.
 */
static inline
void memory_free(void * region)
{
	assert(region);

	free(region);
}

/**
 * Memory error domain.
 *
 * \see error_domain()
 */
extern error_domain_t const memory_error;

/**
 * Memory allocation error description.
 *
 * \see error_reason()
 */
extern error_reason_t const memory_error_alloc;

#endif

/**
 * \}
 */
