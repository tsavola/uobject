/*
 * Copyright 2006  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

/**
 * \defgroup vector Vector
 * \{
 *
 * \code
 * #include <uobject/vector.h>
 * // Pollutes the vector_*, object_* and error_* name spaces.
 * \endcode
 *
 * Dynamically sized containers with linear storage.
 *
 * \par Example of instantiation using Object Vector
 *
 * \code
 * #include <uobject/error.h>
 * #include <uobject/vector.h>
 * #include <stdio.h>
 *
 * // Imaginary picture object.
 * #include "picture.h"
 * 
 * void real_main(int argc, char ** argv, struct error * error)
 * {
 *         object_scoped_new(vector_object, pictures);
 *         object_scoped_new(vector_object, bad);
 *
 *         vector_object_init(pictures, picture);
 *         vector_object_reserve(pictures, argc - 1, error);
 *
 *         printf("Loading pictures...\n");
 *
 *         for (int i = 1; i < argc; i++) {
 *                 struct picture * pic;
 *
 *                 pic = vector_object_push(pictures, error);
 *                 error_goto_if_set (error, clean);
 *
 *                 picture_load(pic, argv[i], error);
 *                 error_goto_if_set (error, clean);
 *
 *                 if (picture_width(pic) == 0 || picture_height(pic) == 0) {
 *                         vector_object_push_copy(bad, pic, error);
 *                         error_goto_if_set (error, clean);
 *
 *                         vector_object_pop(pictures);
 *                 }
 *         }
 *
 *         printf("%d bad files\n", vector_object_size(bad));
 *         vector_object_clear(bad);
 *
 *         printf("Showing pictures...\n");
 *
 *         while (vector_object_size(pictures) > 0) {
 *                 struct picture const * pic;
 *
 *                 pic = vector_object_index(pictures, 0);
 *                 picture_show(pic);
 *
 *                 vector_object_remove(pictures, 0);
 *         }
 *
 * clean:
 *         object_scoped_delete(bad);
 *         object_scoped_delete(pictures);
 * }
 *
 * int main(int argc, char ** argv)
 * {
 *         int retval = 0;
 *         object_scoped_new(error, error);
 *
 *         real_main(argc, argv, error);
 *
 *         if (error_is_set(error)) {
 *                 fprintf(stderr, "Error: %s\n", error_reason(error));
 *                 retval = 1;
 *         }
 *
 *         object_scoped_delete(error);
 *         return retval;
 * }
 * \endcode
 */


#ifndef UOBJECT_VECTOR_H
#define UOBJECT_VECTOR_H

#include "error.h"
#include "object.h"
#include <stddef.h>
#include <stdint.h>
#include <assert.h>

typedef int (* vector_data_comparison_t)(void const *,
                                         void const *);

struct vector_data
{
	size_t size;
	size_t capacity;
	uint8_t * buffer;
};

void vector_data_construct(struct vector_data *);

void vector_data_destruct(struct vector_data const *);

void vector_data_copy(struct vector_data *,
                      struct vector_data const * original,
                      struct error *);

static inline
size_t vector_data_size(struct vector_data const * this)
{
	assert(this);

	return this->size;
}

static inline
uint8_t const * vector_data_buffer(struct vector_data const * this)
{
	assert(this);
	assert(this->size > 0);

	return this->buffer;
}

static inline
void * vector_data_index(struct vector_data const * this,
                         size_t offset)
{
	assert(this);
	assert(offset < this->size);

	return this->buffer + offset;
}

void vector_data_reserve(struct vector_data *,
                         size_t capacity,
                         struct error *);

static inline
void vector_data_clear(struct vector_data * this)
{
	assert(this);

	this->size = 0;
}

void * vector_data_push(struct vector_data *,
                        void const * item_data,
                        size_t item_size,
                        struct error *);

void vector_data_pop(struct vector_data *,
                     void * item_data,
                     size_t item_size);

void * vector_data_insert(struct vector_data *,
                          size_t offset,
                          void const * item_data,
                          size_t item_size,
                          struct error *);

void vector_data_remove(struct vector_data *,
                        size_t offset,
                        void * item_data,
                        size_t item_size);

void vector_data_sort(struct vector_data *,
                      vector_data_comparison_t,
                      size_t item_size);

/**
 * \defgroup vector_pointer_object Pointer Vector object
 * \{
 *
 * Pointer container.
 *
 * \note The pointers can point to anything, including objects.
 */

/**
 * Define the relative order of two entities through pointers.
 *
 * \retval <0 if \a left comes before \a right
 * \retval  0 if \a left is equal to \a right
 * \retval >0 if \a left comes after \a right
 */
typedef int (* vector_pointer_comparison_t)(void const * const * left,
                                            void const * const * right);

/**
 * Pointer Vector data type.
 */
struct vector_pointer
{
	struct vector_data data;
};

static inline
void vector_pointer_construct(struct vector_pointer * this)
{
	vector_data_construct(&this->data);
}

static inline
void vector_pointer_destruct(struct vector_pointer const * this)
{
	vector_data_destruct(&this->data);
}

/**
 * Copy the pointers held by \a original to \a this.
 *
 * \param this
 * \param original is the source.
 * \param error is an unset \link error_object Error\endlink to be set on
 *              failure.
 */
static inline
void vector_pointer_copy(struct vector_pointer * this,
                         struct vector_pointer const * original,
                         struct error * error)
{
	vector_data_copy(&this->data, &original->data, error);
}

/**
 * The length of the vector.
 *
 * \return Number of held pointers.
 */
static inline
unsigned int vector_pointer_size(struct vector_pointer const * this)
{
	return vector_data_size(&this->data) / sizeof (void *);
}

/**
 * The raw data.
 *
 * \return Read-only buffer, or \c NULL if \a this is empty.
 */
static inline
void * const * vector_pointer_buffer(struct vector_pointer const * this)
{
	return (void **) vector_data_buffer(&this->data);
}

/**
 * Get a pointer to a held pointer.
 *
 * \param this
 * \param index of a pointer.
 *
 * \return A pointer to a pointer.
 */
static inline
void ** vector_pointer_index(struct vector_pointer const * this,
                             unsigned int index)
{
	return vector_data_index(&this->data, index * sizeof (void *));
}

/**
 * Allocate storage for at least a specified number of pointers.
 *
 * \param this
 * \param count of pointers to reserve room for.
 * \param error is an unset \link error_object Error\endlink to be set on
 *              failure.
 */
static inline
void vector_pointer_reserve(struct vector_pointer * this,
                            unsigned int count,
                            struct error * error)
{
	vector_data_reserve(&this->data, count * sizeof (void *), error);
}

/**
 * Remove all held pointers.
 */
static inline
void vector_pointer_clear(struct vector_pointer * this)
{
	vector_data_clear(&this->data);
}

/**
 * Appends a pointer at the back.
 *
 * \param this
 * \param pointer to be added.
 * \param error is an unset \link error_object Error\endlink to be set on
 *              failure.
 */
static inline
void vector_pointer_push(struct vector_pointer * this,
                         void * const pointer,
                         struct error * error)
{
	vector_data_push(&this->data, &pointer, sizeof (void *), error);
}

/**
 * Remove the pointer at the back.
 *
 * \pre \a this must not be empty.
 */
static inline
void * vector_pointer_pop(struct vector_pointer * this)
{
	void * pointer;
	vector_data_pop(&this->data, &pointer, sizeof (void *));
	return pointer;
}

/**
 * Inserts a pointer at \a index.
 *
 * \param this
 * \param index is the position of the new pointer.
 * \param pointer to be added.
 * \param error is an unset \link error_object Error\endlink to be set on
 *              failure.
 */
static inline
void vector_pointer_insert(struct vector_pointer * this,
                           unsigned int index,
                           void * const pointer,
                           struct error * error)
{
	vector_data_insert(&this->data, index * sizeof (void *), &pointer, sizeof (void *), error);
}

/**
 * Remove the pointer at \a index.
 *
 * \param this
 * \param index is the position of a pointer.
 *
 * \pre \a this must not be empty.
 */
static inline
void * vector_pointer_remove(struct vector_pointer * this,
                             unsigned int index)
{
	void * pointer;
	vector_data_remove(&this->data, index * sizeof (void *), &pointer, sizeof (void *));
	return pointer;
}

/**
 * Replace a pointer at \a index.
 *
 * \param this
 * \param index is the position of a pointer.
 * \param pointer is the new pointer.
 */
static inline
void vector_pointer_set(struct vector_pointer * this,
                        unsigned int index,
                        void * pointer)
{
	*vector_pointer_index(this, index) = pointer;
}

/**
 * Read the pointer at \a index.
 *
 * \param this
 * \param index is the position of a pointer.
 *
 * \return The pointer.
 */
static inline
void * vector_pointer_get(struct vector_pointer const * this,
                          unsigned int index)
{
	return *vector_pointer_index(this, index);
}

/**
 * Sort the pointers using \c qsort(3).
 *
 * \param this
 * \param compare defines the sorting order.
 */
static inline
void vector_pointer_sort(struct vector_pointer * this,
                         vector_pointer_comparison_t compare)
{
	vector_data_sort(&this->data, (vector_data_comparison_t) compare, sizeof (void *));
}

OBJECT_DEFINE_TYPE(vector_pointer, struct vector_pointer);
OBJECT_DECLARE_CLASS(vector_pointer);

/**
 * \}
 */

/**
 * \defgroup vector_value_object Value Vector object
 * \{
 *
 * Typed container for plain-old-data (without \link object_semantics object
 * semantics\endlink).
 */

/**
 * Value Vector data type.
 */
struct vector_value
{
	struct vector_data data;
};

static inline
void vector_value_construct(struct vector_value * this)
{
	vector_data_construct(&this->data);
}

static inline
void vector_value_destruct(struct vector_value const * this)
{
	vector_data_destruct(&this->data);
}

/**
 * Copy the values held by \a original to \a this.
 *
 * \param Type
 * \param This
 * \param Original is the source.
 * \param Error is an unset \link error_object Error\endlink to be set on
 *              failure.
 */
#define vector_value_copy(Type, This, Original, Error) \
	vector_data_copy( \
		&(This)->data, \
		&(Original)->data, \
		Error)

/**
 * The length of the vector.
 *
 * \return Number of held values.
 */
#define vector_value_size(Type, This) \
	(vector_data_size(&(This)->data) / sizeof (Type))

/**
 * The raw data.
 *
 * \return Read-only buffer, or \c NULL if \a this is empty.
 */
#define vector_value_buffer(Type, This) \
	((Type const *) vector_data_buffer( \
		&(This)->data))

/**
 * Get a pointer to a held value.
 *
 * \param Type
 * \param This
 * \param Index of the value.
 *
 * \return Value pointer.
 */
#define vector_value_index(Type, This, Index) \
	((Type *) vector_data_index( \
		&(This)->data, \
		(Index) * sizeof (Type)))

/**
 * Allocate storage for at least a specified number of values.
 *
 * \param Type
 * \param This
 * \param Count of values to reserve room for.
 * \param Error is an unset \link error_object Error\endlink to be set on
 *              failure.
 */
#define vector_value_reserve(Type, This, Count, Error) \
	vector_data_reserve( \
		&(This)->data, \
		(Count) * sizeof (Type), \
		Error)

/**
 * Remove all held values.
 */
#define vector_value_clear(Type, This) \
	vector_data_clear(&(This)->data)

/**
 * Appends a value at the back.
 *
 * \param Type
 * \param This
 * \param Value to be added.
 * \param Error is an unset \link error_object Error\endlink to be set on
 *              failure.
 */
#define vector_value_push(Type, This, Value, Error) { \
		const Type vector_value_data = Value; \
		vector_data_push( \
			&(This)->data, \
			&vector_value_data, \
			sizeof (Type), Error); \
	}

/**
 * Remove the value at the back.
 *
 * \param Type
 * \param This
 * \param ValuePointer is the location for writing the removed value,
 *                     or \c NULL if it is not needed.
 */
#define vector_value_pop(Type, This, ValuePointer) { \
		Type * const vector_value_pointer = ValuePointer; \
		vector_data_pop( \
			&(This)->data, \
			vector_value_pointer, \
			sizeof (Type)); \
	}

/**
 * Inserts a value at \a Index.
 *
 * \param Type
 * \param This
 * \param Index is the position of the new value.
 * \param Value to be added.
 * \param Error is an unset \link error_object Error\endlink to be set on
 *              failure.
 */
#define vector_value_insert(Type, This, Index, Value, Error) { \
		const Type vector_value_data = Value; \
		vector_data_insert( \
			&(This)->data, \
			(Index) * sizeof (Type), \
			&vector_value_data, \
			sizeof (Type), \
			Error); \
	}

/**
 * Remove the value at \a Index.
 *
 * \param Type
 * \param This
 * \param Index is the position of a value.
 * \param ValuePointer is the location for writing the removed value,
 *                     or \c NULL if it is not needed.
 */
#define vector_value_remove(Type, This, Index, ValuePointer) { \
		Type * const vector_value_pointer = ValuePointer; \
		vector_data_remove( \
			&(This)->data, \
			(Index) * sizeof (Type), \
			vector_value_pointer, \
			sizeof (Type)); \
	}

/**
 * Replace a value at \a Index.
 *
 * \param Type
 * \param This
 * \param Index is the position of a value.
 * \param Value is the new value.
 */
#define vector_value_set(Type, This, Index, Value) \
	(*(Type *) vector_data_index( \
		&(This)->data, \
		(Index) * sizeof (Type)) \
	 = (Value))

/**
 * Read the value at \a Index.
 *
 * \param Type
 * \param This
 * \param Index is the position of a value.
 *
 * \return The value.
 */
#define vector_value_get(Type, This, Index) \
	(*(Type *) vector_data_index( \
		&(This)->data, \
		(Index) * sizeof (Type)))

/**
 * Sort the values using \c qsort(3).
 *
 * \param Type
 * \param This
 * \param Compare defines the sorting order.
 */
#define vector_value_sort(Type, This, Compare) { \
		int (* const vector_value_compare)(const Type *, \
		                                   const Type *) \
			= Compare; \
		vector_data_sort( \
			This, \
			(vector_data_comparison_t) vector_value_compare, \
			sizeof (Type)); \
	}

OBJECT_DEFINE_TYPE(vector_value, struct vector_value);
OBJECT_DECLARE_CLASS(vector_value);

/**
 * \}
 */

/**
 * \defgroup vector_object_object Object Vector object
 * \{
 *
 * Container for data with \link object_semantics object semantics\endlink.
 * Objects are instantiated directly in the container's storage; their
 * lifetime is controlled by the container.  In particular, all held
 * instances are deleted when the vector is destroyed.
 *
 * \note An object type has to be associated with an object vector using
 *       vector_object_init() or vector_object_copy() before it can be used.
 */

/**
 * Define the relative order of two object instances.
 *
 * \retval <0 if \a left comes before \a right
 * \retval  0 if \a left is equal to \a right
 * \retval >0 if \a left comes after \a right
 */
typedef int (* vector_object_comparison_t)(object_const_instance_t left,
                                           object_const_instance_t right);

/**
 * Object Vector data type.
 */
struct vector_object
{
	struct vector_data data;
	struct object_class const * object;
};

void vector_object_construct(struct vector_object *);
void vector_object_destruct(struct vector_object const *);

/**
 * Sets the object type.
 *
 * \param This is the ::vector_object pointer.
 * \param Name of the object.
 *
 * \pre \a This has not been previously initialized.
 *
 * \post \a This can be used to manage instances of the \a Name object.
 *
 * \see vector_object_copy() can also be used to initialize the vector.
 */
#define vector_object_init(This, Name) \
	vector_object_init_class(This, &Name##_object_class)

static inline
void vector_object_init_class(struct vector_object * this,
                              struct object_class const * object)
{
	assert(this);
	assert(this->data.size == 0);
	assert(this->object == NULL);
	assert(object);
	assert(object->size > 0);
	assert(object->construct);
	assert(object->destruct);

	this->object = object;
}

/**
 * The class of the object type being managed.  May be used to check if a
 * vector has not been \link vector_object_init() initialized\endlink yet.
 *
 * \return The class, or \c NULL if \a this has not been initialized.
 */
static inline
struct object_class const * vector_object_class(struct vector_object const * this)
{
	assert(this);

	return this->object;
}

/**
 * Copy the instances held by \a original to \a this.  The vector will be
 * \link vector_object_init() initialized\endlink automatically if it has not
 * been previously initialized.
 *
 * \param this
 * \param original is the source.
 * \param error is an unset \link error_object Error\endlink to be set on
 *              failure.
 *
 * \pre The object type of the contained instances must be copyable (copy
 *      constructor and copy method must be available).
 *
 * \post If the copy constructor or copy method of the contained instances
 *       causes an error during copying, the state of the vector will be
 *       undefined but valid.  Read: some of the instances were copied
 *       successfully and/or some of the old instances remain intact.  The
 *       vector can be destroyed cleanly.
 */
void vector_object_copy(struct vector_object * this,
                        struct vector_object const * original,
                        struct error * error);

/**
 * The length of the vector.
 *
 * \return Number of held instances.
 */
unsigned int vector_object_size(struct vector_object const * this);

/**
 * The raw data.
 *
 * \return Read-only buffer, or \c NULL if \a this is empty.
 */
static inline
void const * vector_object_buffer(struct vector_object const * this)
{
	assert(this);
	assert(this->object);
	assert(this->data.size > 0);

	return this->data.buffer;
}

/**
 * Get a pointer to a held instance.
 *
 * \param this
 * \param index of an instance.
 *
 * \return A pointer to an instance.
 */
void * vector_object_index(struct vector_object const * this,
                           unsigned int index);

/**
 * Allocate storage for at least a specified number of instances.
 *
 * \param this
 * \param count of instances to reserve room for.
 * \param error is an unset \link error_object Error\endlink to be set on
 *              failure.
 */
void vector_object_reserve(struct vector_object * this,
                           unsigned int count,
                           struct error * error);

/**
 * Delete all held instances.
 */
void vector_object_clear(struct vector_object * this);

/**
 * Create a new instance at the back.
 *
 * \param this
 * \param error is an unset \link error_object Error\endlink to be set on
 *              failure.
 *
 * \return Pointer to the new instance.
 */
object_instance_t vector_object_push(struct vector_object * this,
                                     struct error * error);

/**
 * Create a new instance at the back by copying \a original.
 *
 * \param this
 * \param original is the instance to be copied.
 * \param error is an unset \link error_object Error\endlink to be set on
 *              failure.
 *
 * \return Pointer to the new instance.
 */
object_instance_t vector_object_push_copy(struct vector_object * this,
                                          object_const_instance_t original,
                                          struct error * error);

/**
 * Delete the instance at the back.
 *
 * \pre \a this must not be empty.
 */
void vector_object_pop(struct vector_object * this);

/**
 * Create a new instance at \a index.
 *
 * \param this
 * \param index is the position of the new instance.
 * \param error is an unset \link error_object Error\endlink to be set on
 *              failure.
 *
 * \return Pointer to the new instance.
 */
object_instance_t vector_object_insert(struct vector_object * this,
                                       unsigned int index,
                                       struct error * error);

/**
 * Create a new instance at \a index by copying \a original.
 *
 * \param this
 * \param index is the position of the new instance.
 * \param original is the instance to be copied.
 * \param error is an unset \link error_object Error\endlink to be set on
 *              failure.
 *
 * \return Pointer to the new instance.
 */
object_instance_t vector_object_insert_copy(struct vector_object * this,
                                            unsigned int index,
                                            object_const_instance_t original,
                                            struct error * error);

/**
 * Delete the instance at \a index.
 *
 * \param this
 * \param index is the position of an instance.
 */
void vector_object_remove(struct vector_object * this,
                          unsigned int index);

/**
 * Sort the instances using \c qsort(3).
 *
 * \param this
 * \param compare defines the sorting order.
 */
void vector_object_sort(struct vector_object * this,
                        vector_object_comparison_t compare);

OBJECT_DEFINE_TYPE(vector_object, struct vector_object);
OBJECT_DECLARE_CLASS(vector_object);

/**
 * \}
 */

#endif

/**
 * \}
 */
