/*
 * Copyright 2006  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

/**
 * \defgroup error Error
 * \{
 *
 * \code
 * #include <uobject/error.h>
 * // Pollutes the error_* name space.
 * \endcode
 *
 * Error conditions are indicated using the \link error_object Error\endlink
 * object.  Functions which can cause errors take a pointer to an error
 * instance (as the last parameter by convention).  This includes functions
 * which (potentially) call other functions which actually modify the error
 * status.
 *
 * Applications are designed so that the functions which actually handle
 * error conditions (possibly by ignoring them) instantiate an error object,
 * and others just pass it along.  Even a function which does not handle an
 * error needs to be aware of it.
 *
 * \par Example
 *
 * \code
 * #include <uobject/error.h>
 * #include <uobject/memory.h>
 * #include <stddef.h>
 * #include <unistd.h>
 *
 * // Imaginary I/O functions with error support.
 * int open_file(char const *, struct error *);
 * size_t get_file_size(int, struct error *);
 * void read_file(int, char *, size_t, struct error *);
 * void print_data(char *, size_t, struct error *);
 * 
 * void print_file(char const * path, struct error * error)
 * {
 *         int fd;
 *         size_t size;
 *         char * buffer = NULL;
 *
 *         fd = open_file(path, error);
 *         error_return_if_set (error);
 *
 *         size = get_file_size(fd, error);
 *         error_goto_if_set (error, clean);
 *
 *         buffer = memory_alloc(size, error);
 *         error_goto_if_set (error, clean);
 *
 *         read_file(fd, buffer, size, error);
 *         error_goto_if_set (error, clean);
 *
 *         print_data(buffer, size, error);
 *         // No need to do anything even if error is set.
 *
 * clean:
 *         if (buffer)
 *                 memory_free(buffer);
 *
 *         close(fd);
 * }
 * \endcode
 */

#ifndef UOBJECT_ERROR_H
#define UOBJECT_ERROR_H

#include <stdbool.h>
#include <stddef.h>
#include <assert.h>

/**
 * \defgroup error_object Error object
 * \{
 *
 * The error object contains information about the location and reason of a
 * runtime error and the associated system error number (\c errno).
 *
 * This object can only be instantiated using object_scoped_new() because it
 * does not have an associated object_class.  Also, the error_copy() method
 * does not implement the ::object_copy_method_t semantics.
 *
 * \invariant error_is_set() if and only if error_reason() is non-\c NULL.
 * \n         error_domain() is \c NULL if error_reason() is \c NULL.
 * \n         error_errno() is \c 0 if error_reason() is \c NULL.
 *
 * \default error_is_set() is false.
 */

/**
 * Name of a subsystem.
 */
typedef char const * error_domain_t;

/**
 * Description of a failure.
 */
typedef char const * error_reason_t;

/**
 * Error data type.
 */
struct error
{
	error_domain_t domain;
	error_reason_t reason;
	int errno;
};

/* Do this without OBJECT_DEFINE_TYPE() to avoid circular dependencies. */
typedef struct error error_object_t;

void error_construct(struct error *);

static inline
void error_destruct(struct error const * this)
{
	assert(this);
}

/**
 * Resets error status.
 */
void error_clear(struct error * this);

/**
 * Makes a copy of an error object.
 *
 * \param this
 * \param original is an error instance to be copied.
 *
 * \post error_is_set(), error_domain(), error_reason() and error_errno()
 *       return identical values for both instances.
 */
void error_copy(struct error * this,
                struct error const * original);

/**
 * Sets error status, unless error status was already set.
 *
 * \param this
 * \param domain is the error location (may be \c NULL).
 * \param reason is the error description.
 * \param errno is the value of \c errno immediately after the failing
 *              operation (must be \c 0 if not applicable).
 */
static inline
void error_set(struct error * this,
               error_domain_t domain,
               error_reason_t reason,
               int errno)
{
	assert(this);
	assert(reason);

	if (this->reason == NULL) {
		this->domain = domain;
		this->reason = reason;
		this->errno = errno;
	}
}

/**
 * Error status.
 *
 * \retval false if error is not set.
 * \retval true if error is set.
 */
static inline
bool error_is_set(struct error const * this)
{
	assert(this);

	return this->reason != NULL;
}

/**
 * Inverted error status.
 *
 * \retval false if error is set.
 * \retval true if error is not set.
 */
static inline
bool error_not_set(struct error const * this)
{
	assert(this);

	return this->reason == NULL;
}

/**
 * Error location.
 *
 * \return Name of the subsystem which detected the error
 *         (might be \c NULL).
 */
static inline
error_domain_t error_domain(struct error const * this)
{
	assert(this);

	return this->domain;
}

/**
 * Error description.
 *
 * \return Description of the failure, or \c NULL if error status is not set.
 */
static inline
error_reason_t error_reason(struct error const * this)
{
	assert(this);

	return this->reason;
}

/**
 * System error number.
 *
 * \return The value of \c errno at the time of failure, or \c 0 if error
 *         status is not set or the error was not caused by a system
 *         operation.
 */
static inline
int error_errno(struct error const * this)
{
	assert(this);

	return this->errno;
}

/**
 * \}
 */

/**
 * \defgroup error_control Control utilities
 * \{
 */

/**
 * Return from current function immediately if \a Error is set.
 *
 * \param Error is an ::error pointer.
 */
#define error_return_if_set(Error) { \
		if (error_is_set(Error)) \
			return; \
	}

/**
 * Return \a Value from current function immediately if \a Error is set.
 *
 * \param Error is an ::error pointer.
 * \param Value to \c return.
 */
#define error_return_value_if_set(Error, Value) { \
		if (error_is_set(Error)) \
			return (Value); \
	}

/**
 * Jump to \a Label if \a Error is set.
 *
 * \param Error is an ::error pointer.
 * \param Label to \c goto.
 */
#define error_goto_if_set(Error, Label) { \
		if (error_is_set(Error)) \
			goto Label; \
	}

/**
 * \}
 */

#endif

/**
 * \}
 */
