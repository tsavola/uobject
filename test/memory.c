/*
 * Copyright 2006  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

#include "test.h"
#include <uobject/memory.h>
#include <uobject/error.h>

void test_alloc(struct error * error)
{
	void * ptr;

	ptr = memory_alloc(100, error);
	FATAL(error_not_set(error));
	CHECK(ptr != NULL);

	memory_free(ptr);
}

void test_realloc(struct error * error)
{
	void * ptr1;
	int * ptr2;

	ptr1 = memory_alloc(4, error);
	FATAL(error_not_set(error));

	ptr2 = memory_realloc(ptr1, 4, error);
	FATAL(error_not_set(error));

	CHECK(ptr1 == ptr2);

	*ptr2 = 0x01020304;

	ptr2 = memory_realloc(ptr2, 1024 * 1024, error);
	FATAL(error_not_set(error));

	CHECK(*ptr2 == 0x01020304);

	memory_free(ptr2);
}

void test_suite(void)
{
	struct error error;
	error_construct(&error);

	TEST(alloc)(&error);
	TEST(realloc)(&error);
}
