/*
 * Copyright 2006  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

#ifndef TESTS_INT_H
#define TESTS_INT_H

#include <uobject/error.h>
#include <uobject/object.h>
#include <assert.h>

typedef int OBJECT_TYPE(int);

static inline
void int_construct(int * this)
{
	assert(this);

	*this = 0;
}

static inline
void int_construct_copy(int * this, const int * model, struct error * error)
{
	assert(this);
	assert(model);
	assert(error_not_set(error));

	*this = *model;
}

static inline
void int_destruct(const int * this)
{
	assert(this);
}

static inline
void int_copy(int * this, const int * model, struct error * error)
{
	assert(this);
	assert(model);
	assert(error_not_set(error));

	*this = *model;
}

#endif
