/*
 * Copyright 2006  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

#include "test.h"
#include "dummy.h"
#include "virtual.h"
#include <uobject/object.h>
#include <uobject/error.h>

void test_scoped(struct error * error)
{
	object_scoped_new(dummy, instance);
	CHECK(dummy_instances == 1);
	CHECK(dummy_get_value(instance) == DUMMY_DEFAULT_VALUE);

	dummy_set_value(instance, 100, error);
	FATAL(error_not_set(error));
	CHECK(dummy_get_value(instance) == 100);

	object_scoped_delete(instance);
	CHECK(dummy_instances == 0);
}

void test_dynamic(struct error * error)
{
	struct dummy * instance = NULL;

	instance = object_dynamic_new(dummy, error);
	FATAL(error_not_set(error));
	CHECK(dummy_instances == 1);
	CHECK(dummy_get_value(instance) == DUMMY_DEFAULT_VALUE);

	dummy_set_value(instance, 1000, error);
	FATAL(error_not_set(error));
	CHECK(dummy_get_value(instance) == 1000);

	object_dynamic_delete(dummy, instance);
	CHECK(dummy_instances == 0);
}

void test_virtual(struct error * error)
{
	struct abstract * instance = NULL;

	instance = (struct abstract *) object_dynamic_new(concrete, error);
	FATAL(error_not_set(error));

	abstract_set_value(instance, 10);
	CHECK(abstract_get_value(instance) == 10);

	object_dynamic_delete_virtual((object_virtual_instance_t) instance);
}

void test_copy(struct error * error)
{
	object_scoped_new(dummy, instance1);
	struct dummy * instance2 = NULL;
	struct dummy * instance3 = NULL;
	
	dummy_set_value(instance1, 0xf0000, error);
	FATAL(error_not_set(error));

	instance2 = object_dynamic_new_copy(dummy, instance1, error);
	FATAL(error_not_set(error));

	object_scoped_delete(instance1);

	dummy_set_value(instance2, dummy_get_value(instance2) + 0xf00000, error);
	FATAL(error_not_set(error));

	instance3 = object_dynamic_new_copy(dummy, instance2, error);
	FATAL(error_not_set(error));

	CHECK(dummy_instances == 2);
	CHECK(dummy_get_value(instance3) == 0xff0000);

	object_dynamic_delete(dummy, instance2);
	object_dynamic_delete(dummy, instance3);

	CHECK(dummy_instances == 0);
}

void test_suite(void)
{
	struct error error;
	error_construct(&error);

 	dummy_instances = 0;
	TEST(scoped)(&error);

	dummy_instances = 0;
	TEST(dynamic)(&error);

	dummy_instances = 0;
	TEST(copy)(&error);

	dummy_instances = 0;
	TEST(virtual)(&error);
}
