/*
 * Copyright 2006  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

#include "test.h"
#include <uobject/string.h>
#include <uobject/object.h>
#include <uobject/error.h>
#include <string.h>

void do_assign(struct string * s1, struct string * s2, struct error * error)
{
	string_assign(s1, "foo", error);
	FATAL(error_not_set(error));
	CHECK(strcmp(string_contents(s1), "foo") == 0);

	string_assign_array(s2, "barXX", 3, error);
	FATAL(error_not_set(error));
	CHECK(strcmp(string_contents(s2), "bar") == 0);

	string_assign_string(s1, s2, error);
	FATAL(error_not_set(error));
	CHECK(strcmp(string_contents(s1), string_contents(s2)) == 0);
}

void do_append(struct string * s1, struct string * s2, struct error * error)
{
	string_append(s1, "ASDF", error);
	FATAL(error_not_set(error));
	CHECK(strcmp(string_contents(s1), "ASDF") == 0);

	string_append(s1, "foo", error);
	FATAL(error_not_set(error));
	CHECK(strcmp(string_contents(s1), "ASDFfoo") == 0);

	string_append_array(s2, "QWERTYxx", 6, error);
	FATAL(error_not_set(error));
	CHECK(strcmp(string_contents(s2), "QWERTY") == 0);

	string_append_string(s1, s2, error);
	FATAL(error_not_set(error));
	CHECK(strcmp(string_contents(s1), "ASDFfooQWERTY") == 0);
}

void do_insert(struct string * s1, struct string * s2, struct error * error)
{
	string_insert(s1, 0, "ASDF", error);
	FATAL(error_not_set(error));
	CHECK(strcmp(string_contents(s1), "ASDF") == 0);

	string_insert(s1, 2, "foo", error);
	FATAL(error_not_set(error));
	CHECK(strcmp(string_contents(s1), "ASfooDF") == 0);

	string_insert_array(s2, 0, "qwertyXX", 6, error);
	FATAL(error_not_set(error));
	CHECK(strcmp(string_contents(s2), "qwerty") == 0);

	string_insert_string(s1, 7, s2, error);
	FATAL(error_not_set(error));
	CHECK(strcmp(string_contents(s1), "ASfooDFqwerty") == 0);
}

void do_clear(struct string * s, struct error * error)
{
	string_assign(s, "hohoo", error);
	FATAL(error_not_set(error));

	string_clear(s);
	CHECK(string_length(s) == 0);
	CHECK(strlen(string_contents(s)) == 0);
}

void test_construct(void)
{
	object_scoped_new(string, s);

	CHECK(string_length(s) == 0);
	CHECK(strlen(string_contents(s)) == 0);

	object_scoped_delete(s);
}

void test_assign(struct error * error)
{
	object_scoped_new(string, s1);
	object_scoped_new(string, s2);

	do_assign(s1, s2, error);

	object_scoped_delete(s2);
	object_scoped_delete(s1);
}

void test_append(struct error * error)
{
	object_scoped_new(string, s1);
	object_scoped_new(string, s2);

	do_append(s1, s2, error);

	object_scoped_delete(s2);
	object_scoped_delete(s1);
}

void test_insert(struct error * error)
{
	object_scoped_new(string, s1);
	object_scoped_new(string, s2);

	do_insert(s1, s2, error);

	object_scoped_delete(s2);
	object_scoped_delete(s1);
}

void test_clear(struct error * error)
{
	object_scoped_new(string, s);

	do_clear(s, error);

	object_scoped_delete(s);
}

void test_utf8(struct error * error)
{
	object_scoped_new(string, s);

	CHECK(string_utf8_length(s) == 0);

	char const valid1[] = "Foo Bar";
	string_assign(s, valid1, error);
	CHECK(string_utf8_length(s) == 7);

	char const valid2[] = { 'A', (char) 0xd7, (char) 0x90, 'B', '\0' };
	string_assign(s, valid2, error);
	CHECK(string_utf8_length(s) == 3);

	char const valid3[] = { (char) 0xd7, (char) 0x90, (char) 0xd7, (char) 0x90, '\0' };
	string_assign(s, valid3, error);
	CHECK(string_utf8_length(s) == 2);

	char const invalid1[] = { 'A', (char) 0xe0, (char) 0x80, 'B', 'C', 'D', '\0' };
	string_assign(s, invalid1, error);
	CHECK(string_utf8_length(s) == -1);

	char const invalid2[] = { 'A', 'B', 'C', (char) 0xc0, '\0' };
	string_assign(s, invalid2, error);
	CHECK(string_utf8_length(s) == -1);

	char const invalid3[] = { 'A', (char) 0x80, 'B', 'C', '\0' };
	string_assign(s, invalid3, error);
	CHECK(string_utf8_length(s) == -1);

	object_scoped_delete(s);
}

void test_static_construct(void)
{
	object_scoped_new(string_static, s);

	CHECK(string_static_length(s) == 0);
	CHECK(strlen(string_static_contents(s)) == 0);

	object_scoped_delete(s);
}

void test_static_assign(struct error * error)
{
	object_scoped_new(string_static, s1);
	object_scoped_new(string_static, s2);

	do_assign((struct string *) s1, (struct string *) s2, error);

	object_scoped_delete(s2);
	object_scoped_delete(s1);
}

void test_static_append(struct error * error)
{
	object_scoped_new(string_static, s1);
	object_scoped_new(string_static, s2);

	do_append((struct string *) s1, (struct string *) s2, error);

	object_scoped_delete(s2);
	object_scoped_delete(s1);
}

void test_static_insert(struct error * error)
{
	object_scoped_new(string_static, s1);
	object_scoped_new(string_static, s2);

	do_insert((struct string *) s1, (struct string *) s2, error);

	object_scoped_delete(s2);
	object_scoped_delete(s1);
}

void test_static_clear(struct error * error)
{
	object_scoped_new(string_static, s);

	do_clear((struct string *) s, error);

	object_scoped_delete(s);
}

void test_mixed_assign1(struct error * error)
{
	object_scoped_new(string, s1);
	object_scoped_new(string_static, s2);

	do_assign(s1, (struct string *) s2, error);

	object_scoped_delete(s2);
	object_scoped_delete(s1);
}

void test_mixed_assign2(struct error * error)
{
	object_scoped_new(string_static, s1);
	object_scoped_new(string, s2);

	do_assign((struct string *) s1, s2, error);

	object_scoped_delete(s2);
	object_scoped_delete(s1);
}

void test_mixed_append1(struct error * error)
{
	object_scoped_new(string, s1);
	object_scoped_new(string_static, s2);

	do_append(s1, (struct string *) s2, error);

	object_scoped_delete(s2);
	object_scoped_delete(s1);
}

void test_mixed_append2(struct error * error)
{
	object_scoped_new(string_static, s1);
	object_scoped_new(string, s2);

	do_append((struct string *) s1, s2, error);

	object_scoped_delete(s2);
	object_scoped_delete(s1);
}

void test_mixed_insert1(struct error * error)
{
	object_scoped_new(string, s1);
	object_scoped_new(string_static, s2);

	do_insert(s1, (struct string *) s2, error);

	object_scoped_delete(s2);
	object_scoped_delete(s1);
}

void test_mixed_insert2(struct error * error)
{
	object_scoped_new(string_static, s1);
	object_scoped_new(string, s2);

	do_insert((struct string *) s1, s2, error);

	object_scoped_delete(s2);
	object_scoped_delete(s1);
}

void test_suite(void)
{
	struct error error;
	error_construct(&error);

	TEST(construct)();
	TEST(assign)(&error);
	TEST(append)(&error);
	TEST(insert)(&error);
	TEST(clear)(&error);
	TEST(utf8)(&error);

	TEST(static_construct)();
	TEST(static_assign)(&error);
	TEST(static_append)(&error);
	TEST(static_insert)(&error);
	TEST(static_clear)(&error);

	TEST(mixed_assign1)(&error);
	TEST(mixed_assign2)(&error);
	TEST(mixed_append1)(&error);
	TEST(mixed_append2)(&error);
	TEST(mixed_insert1)(&error);
	TEST(mixed_insert2)(&error);
}
