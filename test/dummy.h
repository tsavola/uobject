/*
 * Copyright 2006  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

#ifndef TEST_DUMMY_H
#define TEST_DUMMY_H

#include <uobject/error.h>
#include <uobject/memory.h>
#include <uobject/object.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>

#define DUMMY_DEFAULT_VALUE 10

static int dummy_instances = 0;

struct dummy
{
	uint32_t * value;
	bool oddity;
};

static inline
void dummy_construct(struct dummy * this)
{
	assert(this);

	this->value = NULL;

	dummy_instances++;
}

static inline
void dummy_destruct(struct dummy const * this)
{
	assert(this);
	assert(this->value == NULL || (*this->value & 1) == this->oddity);

	if (this->value)
		memory_free(this->value);

	dummy_instances--;
}

static inline
void dummy_construct_copy(struct dummy * this,
                          struct dummy const * model,
                          struct error * error)
{
	assert(this);
	assert(model);
	assert(error_not_set(error));

	if (model->value) {
		this->value = memory_alloc(sizeof (uint32_t), error);
		error_return_if_set (error);

		*this->value = *model->value;
		this->oddity = model->oddity;
	} else {
		this->value = NULL;
	}

	dummy_instances++;
}

static inline
void dummy_copy(struct dummy * this,
                struct dummy const * model,
                struct error * error)
{
	assert(this);
	assert(model);
	assert(error_not_set(error));

	if (model->value) {
		if (this->value == NULL) {
			this->value = memory_alloc(sizeof (uint32_t), error);
			error_return_if_set (error);
		}

		*this->value = *model->value;
		this->oddity = model->oddity;
	} else {
		if (this->value) {
			*this->value = DUMMY_DEFAULT_VALUE;
			this->oddity = DUMMY_DEFAULT_VALUE & 1;
		}
	}
}

static inline
void dummy_set_value(struct dummy * this,
                     uint32_t value,
                     struct error * error)
{
	assert(this);
	assert(this->value == NULL || (*this->value & 1) == this->oddity);

	if (this->value == NULL) {
		this->value = memory_alloc(sizeof (uint32_t), error);
		error_return_if_set (error);
	}

	*this->value = value;
	this->oddity = value & 1;
}

static inline
uint32_t dummy_get_value(struct dummy const * this)
{
	assert(this);
	assert(this->value == NULL || (*this->value & 1) == this->oddity);

	return this->value ? *this->value : DUMMY_DEFAULT_VALUE;
}

static inline
uint32_t dummy_is_odd(struct dummy const * this)
{
	assert(this);
	assert(this->value == NULL || (*this->value & 1) == this->oddity);

	return this->value ? this->oddity : (DUMMY_DEFAULT_VALUE & 1);
}

static inline
int dummy_compare(struct dummy const * this,
                  struct dummy const * other)
{
	uint32_t a, b;

	a = dummy_get_value(this);
	b = dummy_get_value(other);

	if (a < b)
		return -1;
	else if (a > b)
		return 1;
	else
		return 0;
}

OBJECT_DEFINE_TYPE(dummy, struct dummy);
static OBJECT_INITIALIZE_COPYABLE_CLASS(dummy);

#endif
