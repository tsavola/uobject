/*
 * Copyright 2006  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

#include "test.h"
#include <uobject/error.h>
#include <uobject/object.h>

const char * const test_domain = "Test domain";
const char * const test_reason = "Test reason";

void test_construct(void)
{
	struct error instance;
	error_construct(&instance);

	CHECK(error_not_set(&instance));
	CHECK(!error_is_set(&instance));
}

void test_set(void)
{
	struct error instance;
	error_construct(&instance);

	error_set(&instance, test_domain, test_reason, 0);

	CHECK(error_is_set(&instance));
	CHECK(!error_not_set(&instance));
}

void test_values(void)
{
	struct error instance;
	error_construct(&instance);

	error_set(&instance, test_domain, test_reason, 0);

	CHECK(error_domain(&instance) == test_domain);
	CHECK(error_reason(&instance) == test_reason);
}

void do_return(bool * ok)
{
	struct error instance;
	error_construct(&instance);

	*ok = false;

	error_return_if_set (&instance);

	*ok = true;

	error_set(&instance, test_domain, test_reason, 0);
	error_return_if_set (&instance);

	*ok = false;
}

bool do_return_value()
{
	struct error instance;
	error_construct(&instance);

	error_return_value_if_set (&instance, false);

	error_set(&instance, test_domain, test_reason, 0);
	error_return_value_if_set (&instance, true);

	return false;
}

void test_return(void)
{
	bool ok;
	do_return(&ok);
	CHECK(ok == true);
}

void test_return_value(void)
{
	CHECK(do_return_value());
}

void test_goto(void)
{
	struct error instance;
	error_construct(&instance);

	error_goto_if_set (&instance, fail);

	error_set(&instance, test_domain, test_reason, 0);
	error_goto_if_set (&instance, pass);

fail:	CHECK(false);
pass:	return;
}

void test_object(void)
{
	object_scoped_new(error, instance);

	CHECK(error_not_set(instance));

	object_scoped_delete(instance);
}

void test_suite(void)
{
	TEST(construct)();
	TEST(set)();
	TEST(values)();

	TEST(return)();
	TEST(return_value)();
	TEST(goto)();

	TEST(object)();
}
