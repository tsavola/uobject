/*
 * Copyright 2006  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

#ifndef TEST_TEST_H
#define TEST_TEST_H

#include <stdio.h>
#include <stdlib.h>

static void test_suite(void);

#ifndef NDEBUG

# define TEST(name) \
	test__current = __STRING(name); \
	test_##name

# define CHECK(expr) { \
		if (!(expr)) { \
			fprintf(stderr, "%s:%d:%s: fail: %s\n", __FILE__, __LINE__, test__current, __STRING(expr)); \
			test__errors++; \
			return; \
		} else { \
			printf("%s:%d:%s: pass\n", __FILE__, __LINE__, test__current); \
			fflush(stdout); \
		} \
	}

# define FATAL(expr) { \
		if (!(expr)) { \
			fflush(stdout); \
			fprintf(stderr, "%s:%d:%s: fatal: %s\n", __FILE__, __LINE__, test__current, __STRING(expr)); \
			exit(127); \
		} else { \
			printf("%s:%d:%s: pass\n", __FILE__, __LINE__, test__current); \
		} \
	}

static const char * test__current = NULL;
static int test__errors = 0;

int main(void)
{
	test_suite();
	return test__errors < 128 ? test__errors : 127;
}

#else

# define TEST(name) test_##name
# define CHECK(expr) { if (!(expr)) abort(); }
# define FATAL(expr) { if (!(expr)) abort(); }

int main(void)
{
	test_suite();
	return 0;
}

#endif

#endif
