/*
 * Copyright 2006  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

#include "test.h"
#include "dummy.h"
#include <uobject/vector.h>
#include <uobject/object.h>
#include <uobject/error.h>

/* value vector */

void test_value_push(struct error * error)
{
	object_scoped_new(vector_value, instance);

	vector_value_push(int, instance, 12345, error);
	FATAL(error_not_set(error));

	CHECK(vector_value_size(int, instance) == 1);
	CHECK(vector_data_size((struct vector_data *) instance) == sizeof (int));

	CHECK(vector_value_get(int, instance, 0) == 12345);

	object_scoped_delete(instance);
}

void test_value_pop(struct error * error)
{
	object_scoped_new(vector_value, instance);

	vector_value_push(int, instance, 12345, error);
	FATAL(error_not_set(error));

	vector_value_pop(int, instance, NULL);
	CHECK(vector_value_size(int, instance) == 0);

	object_scoped_delete(instance);
}

/* pointer vector */

void test_pointer_push(struct error * error)
{
	object_scoped_new(vector_pointer, instance);
	int data = 100;

	vector_pointer_push(instance, &data, error);
	FATAL(error_not_set(error));

	CHECK(vector_pointer_size(instance) == 1);
	CHECK(vector_data_size((struct vector_data *) instance) == sizeof (int *));

	CHECK(vector_pointer_get(instance, 0) == &data);

	object_scoped_delete(instance);
}

void test_pointer_pop(struct error * error)
{
	object_scoped_new(vector_pointer, instance);
	int data = 100;

	vector_pointer_push(instance, &data, error);
	FATAL(error_not_set(error));

	CHECK(vector_pointer_pop(instance) == &data);
	CHECK(vector_pointer_size(instance) == 0);

	object_scoped_delete(instance);
}

/* object vector */

void test_object_init()
{
	object_scoped_new(vector_object, instance);

	vector_object_init(instance, dummy);
	CHECK(vector_object_size(instance) == 0);

	object_scoped_delete(instance);
}

void test_object_push(struct error * error)
{
	object_scoped_new(vector_object, instance);
	vector_object_init(instance, dummy);

	vector_object_push(instance, error);
	FATAL(error_not_set(error));
	CHECK(vector_object_size(instance) == 1);
	CHECK(dummy_instances == 1);

	vector_object_push(instance, error);
	FATAL(error_not_set(error));
	CHECK(vector_object_size(instance) == 2);
	CHECK(dummy_instances == 2);

	struct dummy * item;

	item = vector_object_index(instance, 0);
	CHECK(dummy_get_value(item) == DUMMY_DEFAULT_VALUE);

	item = vector_object_index(instance, 1);
	CHECK(dummy_get_value(item) == DUMMY_DEFAULT_VALUE);

	object_scoped_delete(instance);
	CHECK(dummy_instances == 0);
}

void test_object_push_copy(struct error * error)
{
	object_scoped_new(vector_object, instance);
	vector_object_init(instance, dummy);

	object_scoped_new(dummy, item1);

	dummy_set_value(item1, 0x04050607, error);
	FATAL(error_not_set(error));

	vector_object_push_copy(instance, item1, error);
	FATAL(error_not_set(error));
	CHECK(vector_object_size(instance) == 1);
	CHECK(dummy_instances == 1 + 1);

	dummy_set_value(item1, 0x07060504, error);
	FATAL(error_not_set(error));

	vector_object_push_copy(instance, item1, error);
	FATAL(error_not_set(error));
	CHECK(vector_object_size(instance) == 2);
	CHECK(dummy_instances == 1 + 2);

	struct dummy * item2;

	item2 = vector_object_index(instance, 0);
	CHECK(dummy_get_value(item2) == 0x04050607);

	item2 = vector_object_index(instance, 1);
	CHECK(dummy_get_value(item2) == 0x07060504);

	object_scoped_delete(instance);
	CHECK(dummy_instances == 1);

	object_scoped_delete(item1);
	CHECK(dummy_instances == 0);
}

void do_populate(struct vector_object * instance,
                 unsigned int size,
                 struct error * error)
{
	struct dummy * item;

	vector_object_init(instance, dummy);

	for (unsigned int i = 0; i < size; i++) {
		item = vector_object_push(instance, error);
		FATAL(error_not_set(error));

		dummy_set_value(item, i, error);
		FATAL(error_not_set(error));
	}
}

void test_object_insert(struct error * error)
{
	object_scoped_new(vector_object, instance);
	struct dummy * item;

	do_populate(instance, 10, error);

	vector_object_insert(instance, 4, error);
	FATAL(error_not_set(error));
	CHECK(vector_object_size(instance) == 11);

	vector_object_insert(instance, 0, error);
	FATAL(error_not_set(error));
	CHECK(vector_object_size(instance) == 12);

	vector_object_insert(instance, 12, error);
	FATAL(error_not_set(error));
	CHECK(vector_object_size(instance) == 13);

	item = vector_object_index(instance, 5);
	CHECK(dummy_get_value(item) == DUMMY_DEFAULT_VALUE);

	item = vector_object_index(instance, 0);
	CHECK(dummy_get_value(item) == DUMMY_DEFAULT_VALUE);

	item = vector_object_index(instance, 12);
	CHECK(dummy_get_value(item) == DUMMY_DEFAULT_VALUE);

	CHECK(dummy_instances == 13);

	object_scoped_delete(instance);
	CHECK(dummy_instances == 0);
}

void test_object_insert_copy(struct error * error)
{
	object_scoped_new(vector_object, instance);
	object_scoped_new(dummy, item1);
	struct dummy * item2;

	do_populate(instance, 10, error);

	CHECK(dummy_instances == 1 + 10);

	dummy_set_value(item1, 0xff00ff00, error);
	FATAL(error_not_set(error));

	vector_object_insert_copy(instance, 4, item1, error);
	FATAL(error_not_set(error));
	CHECK(vector_object_size(instance) == 11);

	item2 = vector_object_index(instance, 4);
	CHECK(dummy_get_value(item2) == 0xff00ff00);

	CHECK(dummy_instances == 1 + 11);

	object_scoped_delete(item1);
	CHECK(dummy_instances == 11);

	object_scoped_delete(instance);
	CHECK(dummy_instances == 0);
}

void test_object_pop(struct error * error)
{
	object_scoped_new(vector_object, instance);
	struct dummy * item;

	do_populate(instance, 10, error);

	for (int i = 10 - 1; i >= 0; i--) {
		item = vector_object_index(instance, 0);
		CHECK(dummy_get_value(item) == 0);

		vector_object_pop(instance);
		CHECK(vector_object_size(instance) == (size_t) i);
		CHECK(dummy_instances == i);
	}
}

void test_object_remove(struct error * error)
{
	object_scoped_new(vector_object, instance);
	struct dummy * item;

	do_populate(instance, 10, error);

	vector_object_remove(instance, 0);
	CHECK(vector_object_size(instance) == 9);
	CHECK(dummy_instances == 9);

	item = vector_object_index(instance, 0);
	CHECK(dummy_get_value(item) == 1);

	vector_object_remove(instance, 5);
	CHECK(vector_object_size(instance) == 8);
	CHECK(dummy_instances == 8);

	item = vector_object_index(instance, 4);
	CHECK(dummy_get_value(item) == 5);

	item = vector_object_index(instance, 5);
	CHECK(dummy_get_value(item) == 7);
}

void test_object_clear(struct error * error)
{
	object_scoped_new(vector_object, instance);

	do_populate(instance, 10, error);
	CHECK(dummy_instances == 10);

	vector_object_clear(instance);
	CHECK(vector_object_size(instance) == 0);
	CHECK(dummy_instances == 0);

	object_scoped_delete(instance);
	CHECK(dummy_instances == 0);
}

void test_object_copy(struct error * error)
{
	object_scoped_new(vector_object, instance1);
	object_scoped_new(vector_object, instance2);

	do_populate(instance1, 10, error);
	vector_object_init(instance2, dummy);

	vector_object_copy(instance2, instance1, error);
	FATAL(error_not_set(error));
	CHECK(vector_object_size(instance2) == 10);
	CHECK(dummy_instances == 20);

	vector_object_copy(instance1, instance2, error);
	FATAL(error_not_set(error));
	CHECK(dummy_instances == 20);

	vector_object_clear(instance1);
	CHECK(dummy_instances == 10);

	vector_object_copy(instance2, instance1, error);
	FATAL(error_not_set(error));
	CHECK(vector_object_size(instance2) == 0);
	CHECK(dummy_instances == 0);

	object_scoped_delete(instance2);
	object_scoped_delete(instance1);
	CHECK(dummy_instances == 0);
}

int do_not_call(object_const_instance_t a,
                object_const_instance_t b)
{
	FATAL(false);
	return 0;
}

void test_object_sort(struct error * error)
{
	object_scoped_new(vector_object, instance);
	struct dummy * item;

	vector_object_init(instance, dummy);

	vector_object_sort(instance, do_not_call);

	item = vector_object_push(instance, error);
	FATAL(error_not_set(error));
	dummy_set_value(item, 2, error);
	FATAL(error_not_set(error));

	item = vector_object_push(instance, error);
	FATAL(error_not_set(error));
	dummy_set_value(item, 1, error);
	FATAL(error_not_set(error));

	vector_object_sort(instance, (vector_object_comparison_t) dummy_compare);

	CHECK(dummy_get_value(vector_object_index(instance, 0)) == 1);
	CHECK(dummy_get_value(vector_object_index(instance, 1)) == 2);

	object_scoped_delete(instance);
}

/* test suite */

void test_suite(void)
{
	struct error error;
	error_construct(&error);

	TEST(value_push)(&error);
	TEST(value_pop)(&error);

	TEST(pointer_push)(&error);
	TEST(pointer_pop)(&error);

	dummy_instances = 0;  TEST(object_init)();
	dummy_instances = 0;  TEST(object_push)(&error);
	dummy_instances = 0;  TEST(object_push_copy)(&error);
	dummy_instances = 0;  TEST(object_insert)(&error);
	dummy_instances = 0;  TEST(object_insert_copy)(&error);
	dummy_instances = 0;  TEST(object_pop)(&error);
	dummy_instances = 0;  TEST(object_remove)(&error);
	dummy_instances = 0;  TEST(object_clear)(&error);
	dummy_instances = 0;  TEST(object_copy)(&error);
	dummy_instances = 0;  TEST(object_sort)(&error);
}
