/*
 * Copyright 2006  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

#ifndef TEST_VIRTUAL_H
#define TEST_VIRTUAL_H

#include <uobject/error.h>
#include <uobject/object.h>
#include <stdint.h>
#include <assert.h>

/* abstract object */

struct abstract
{
	struct object_virtual_base virtual;
};

typedef void (* abstract_set_value_t)(struct abstract *, int32_t);
typedef int32_t (* abstract_get_value_t)(struct abstract const *);

struct abstract_virtual_table
{
	struct object_virtual_table virtual;
	abstract_set_value_t set_value;
	abstract_get_value_t get_value;
};

void abstract_set_virtual_table(struct abstract * this,
                                struct abstract_virtual_table const * table)
{
	this->virtual.table = (struct object_virtual_table *) table;
}

struct abstract_virtual_table const * abstract_get_virtual_table(struct abstract const * this)
{
	assert(this);
	assert(this->virtual.table);

	return (struct abstract_virtual_table *) this->virtual.table;
}

void abstract_set_value(struct abstract * this,
                        int32_t value)
{
	abstract_get_virtual_table(this)->set_value(this, value);
}

int32_t abstract_get_value(struct abstract const * this)
{
	return abstract_get_virtual_table(this)->get_value(this);
}

/* concrete object */

struct concrete
{
	struct abstract abstract;
	int64_t data;
};

void concrete_virtual_destruct(struct concrete const * this)
{
}

void concrete_virtual_copy(struct concrete * this,
                           struct abstract const * model,
                           struct error * error)
{
	assert(model);
	assert(error_not_set(error));

	this->data = abstract_get_value(model) << 1;
}

void concrete_virtual_set_value(struct concrete * this,
                                int32_t value)
{
	this->data = value << 1;
}

int32_t concrete_virtual_get_value(struct concrete const * this)
{
	return this->data >> 1;
}

struct abstract_virtual_table const concrete_virtual_table = {
	{
		(object_destructor_t) concrete_virtual_destruct,
	},
	(abstract_set_value_t) concrete_virtual_set_value,
	(abstract_get_value_t) concrete_virtual_get_value,
};

void concrete_construct(struct concrete * this)
{
	assert(this);

	abstract_set_virtual_table(&this->abstract, &concrete_virtual_table);
	this->data = 0;
}

void concrete_destruct(struct concrete const * this)
{
	object_virtual_destruct(&this->abstract.virtual);
}

OBJECT_DEFINE_TYPE(concrete, struct concrete);
OBJECT_INITIALIZE_NONCOPYABLE_VIRTUAL_CLASS(concrete);

#endif
