(font-lock-add-keywords
 'c-mode
 '(("[^[:alnum:]_]\\(error_\\(return\\|return_value\\|goto\\)_if_set\\)[[:space:]]*("
    1 font-lock-keyword-face t)
   ("[^[:alnum:]_]error_goto_if_set[[:space:]]*([^,]*,\\([^)]+\\)"
    1 font-lock-constant-face t)
   ("[^[:alnum:]_]object_is_copyable[[:space:]]*([[:space:]]*\\([[:alnum:]_]+\\)[[:space:]]*)"
    1 font-lock-type-face t)
   ("[^[:alnum:]_]object_\\(scoped\\|dynamic\\|virtual\\)_new[[:space:]]*([[:space:]]*\\([[:alnum:]_]+\\)[,)[:space:]]"
    2 font-lock-type-face t)
   ("[^[:alnum:]_]object_scoped_new[[:space:]]*([[:space:]]*[[:alnum:]_]+,[[:space:]]*\\([[:alnum:]_]+\\)[[:space:]]*)"
    1 font-lock-variable-name-face t)
   ("[^[:alnum:]_]object_dynamic_delete[[:space:]]*([[:space:]]*\\([[:alnum:]_]+\\)[[:space:]]*,"
    1 font-lock-type-face t)
   ("[^[:alnum:]_]vector_value_[[:alnum:]_]+[[:space:]]*([[:space:]]*\\([[:alnum:]_]+\\)[,)[:space:]]"
    1 font-lock-type-face t)
   ("[^[:alnum:]_]vector_object_init[[:space:]]*([^,]*,[[:space:]]*\\([[:alnum:]_]+\\)[[:space:]]*)"
    1 font-lock-type-face t)))
